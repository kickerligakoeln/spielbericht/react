import React from 'react';
import {Route, Switch} from "react-router-dom";
import moment from "moment";
import axios from "axios";
import {useAuth0} from "@auth0/auth0-react";

import Landingpage from "Pages/Landingpage/Landingpage";
import History from "Pages/History/History";
import Live from "Pages/Live/Live";
import Enter from "Pages/Enter/Enter";
import Account from "Pages/Account/Account";
import Stats from "Pages/Stats/Stats";
import LoadingFullsize from "Dumb/Loading/LoadingFullsize";
import {Notification} from "Dumb/Notification/Notification";


export default function App() {
    const {isAuthenticated, getAccessTokenSilently} = useAuth0();
    moment.locale('de');

    /* Set authorization header for registered user */
    if (isAuthenticated) {
        getAccessTokenSilently().then(access_token => {
            axios.defaults.headers.common['Authorization'] = access_token;
        });
    }

    return (
        <main>
            <Switch className={'switch'}>
                <Route exact path={'/enter'} component={Enter}/>
                <Route exact path={'/enter/:id'} component={Enter}/>
                <Route exact path={'/history'} component={History}/>
                <Route exact path={'/live/:id'} component={Live}/>
                <Route exact path={'/account'} component={Account}/>
                <Route exact path={'/performance'} component={Stats}/>
                <Route path={'/'} component={Landingpage}/>
            </Switch>

            <LoadingFullsize/>
            <Notification/>
        </main>
    )
}
