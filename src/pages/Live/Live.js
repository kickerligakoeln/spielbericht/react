import React from "react";
import {connect} from 'react-redux'

import {showPageLoading, hidePageLoading} from "Redux/loadingStore";
import {setScoresheetStore, setPlayerPerformance} from "Redux/scoresheetStore";

import moment from "moment";
import Scoresheet from "JS/rest/Scoresheet";
import {GameType} from "JS/enums/GameType";
import {Helmet} from "react-helmet";
import {Redirect} from "react-router-dom";
import Slider from "react-slick";

import {Topbar} from "Dumb/Topbar/Topbar";
import {Footerbar} from "Dumb/Footerbar/Footerbar";
import {ScoresheetTitle} from "Dumb/Title/Title";
import {ScoresheetDraw} from "Dumb/Draw/Draw";
import {ScoresheetTrend} from "Dumb/Trend/Trend";
import {ScoresheetGamedata} from "Dumb/Gamedata/Gamedata";
import {ScoresheetPlayers} from "Dumb/Players/Players";
import {ScoresheetShare} from "Dumb/Share/Share";
import {ScoresheetMap} from "Dumb/Map/Map";
import LiveTable from "Smart/LiveTable/LiveTable";
import LiveGames from "Smart/LiveGames/LiveGames";
import {AdminEditScoresheet} from "Smart/Admin/EditScoresheet";

import "./detail.scss";
import HeadToHead from "Smart/Live/HeadToHead";

class Live extends React.Component {
    static defaultProps = {
        pagination: ['Spielbericht', 'Spielinfo', 'Tabelle']
    };

    constructor(props) {
        super(props);

        this.state = {
            isLoaded: false
        }
    }


    componentDidMount() {
        this.props.showPageLoading('Loading data');

        Scoresheet.get.id(this.props.match.params.id)
            .then(response => {
                if (!response) {
                    this.setState({
                        error: true,
                        isLoaded: true
                    });
                } else {
                    this.props.setScoresheetStore(response.scoresheet);
                    this.props.setPlayerPerformance(response.performance.player);
                    this.setState({
                        scoresheet: response.scoresheet,
                        isLoaded: true
                    });
                }

                console.info('Scoresheet', response.scoresheet, response.performance);
                this.props.hidePageLoading();
            });
    }

    render() {
        const {error, scoresheet} = this.state;
        const settings = {
            dots: true,
            autoplay: false,
            arrows: false,
            infinite: false,
            speed: 330,
            slidesToShow: 1,
            slidesToScroll: 1,
            customPaging: (i) => {
                return (
                    <span>{this.props.pagination[i]}</span>
                );
            },
        };

        if (!this.state.isLoaded) {
            return (<div className="blank-page"></div>);
        } else {
            if (error) {
                return (
                    <Redirect to='/history'/>
                );
            } else {
                return (
                    <div id="detail">
                        {(scoresheet) ? (
                            <Helmet>
                                <title>{`${scoresheet.team_home.name} vs ${scoresheet.team_guest.name} | Kölner Kickerliga`}</title>
                                <meta property="og:url"
                                      content={`${window.location.origin}/live/${scoresheet.matchid}`}/>
                                <meta property="og:description"
                                      content={`${scoresheet.team_home.name} vs ${scoresheet.team_guest.name} (${scoresheet.team_home.set}:${scoresheet.team_guest.set})`}/>
                            </Helmet>
                        ) : ''}

                        <Topbar />

                        <div className="detail">
                            <ScoresheetTitle/>
                            <AdminEditScoresheet />

                            <Slider {...settings}>
                                <div data-title="Spielbericht">
                                    <section className="container">
                                        <div className="section-head">
                                            <h1>Ergebnisse</h1>
                                        </div>
                                        <ScoresheetDraw/>
                                    </section>

                                    <section className="container">
                                        <div className="section-head">
                                            <h1>Spielverlauf</h1>
                                        </div>

                                        <ScoresheetTrend/>
                                    </section>
                                </div>

                                <div data-title="Spieldaten">
                                    <section className="container">
                                        <div className="section-head">
                                            {(scoresheet.gameType === GameType.GAMEDAY) ? (
                                                <h1>Ligaspiel</h1>
                                            ) : null}

                                            {(scoresheet.gameType === GameType.CUP) ? (
                                                <h1>Pokalspiel</h1>
                                            ) : null}

                                            {(scoresheet.gameType === GameType.CUSTOM) ? (
                                                <h1>Freundschaftsspiel</h1>
                                            ) : null}
                                        </div>
                                        <ScoresheetGamedata/>
                                    </section>

                                    <hr/>
                                    <section className="container">
                                        <div data-grid="2">
                                            <ScoresheetPlayers team="home"/>
                                            <ScoresheetPlayers team="guest"/>
                                        </div>
                                    </section>

                                    <hr/>
                                    <section className="container">
                                        <div className="section-head"><h1>Direktvergleich</h1></div>

                                        <HeadToHead
                                            matchid={scoresheet.matchid}
                                            team_home={scoresheet.team_home.name}
                                            team_guest={scoresheet.team_guest.name} />
                                    </section>
                                </div>

                                {(scoresheet && scoresheet.gameType === GameType.GAMEDAY) ? (
                                    <div data-title="Tabelle">
                                        <section className="container">
                                            <div className="section-head">
                                                <h1>{scoresheet.matchInfo.leagueName}</h1>
                                            </div>
                                            <LiveTable season={scoresheet.matchInfo.currentSeason}/>
                                        </section>

                                        <section className="container">
                                            <div className="section-head">
                                                <h1>Laufender Spieltag</h1>
                                            </div>
                                            <LiveGames season={moment(scoresheet.date.create).format('YYYY')}
                                                       gameday={scoresheet.matchInfo.gamedayNumber}
                                                       gameType={GameType.GAMEDAY}
                                                       league={scoresheet.matchInfo.leagueCode}/>
                                        </section>
                                    </div>
                                ) : ''}
                            </Slider>

                            <ScoresheetMap/>
                            <ScoresheetShare/>
                        </div>

                        <div className="-spacer"></div>
                        <Footerbar/>
                    </div>
                );
            }
        }
    }

}

const mapStateToProps = state => ({
    loadingStore: state.loadingStore,
    scoresheetStore: state.scoresheetStore
});

export default connect(mapStateToProps, {
    showPageLoading,
    hidePageLoading,
    setScoresheetStore,
    setPlayerPerformance
})(Live);
