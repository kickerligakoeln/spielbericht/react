import React from "react";
import {Link} from "react-router-dom";
import moment from "moment";
import Store from "JS/utils/Store";
import {Topbar} from "Dumb/Topbar/Topbar";
import {Footerbar} from "Dumb/Footerbar/Footerbar";
import Scoresheet from "JS/rest/Scoresheet";

import imgLive from "Images/bg-live.jpg"
import imgCreate from "Images/bg-create.jpg"
import "./landingpage.scss"
import Tracking from "JS/rest/Tracking";
import {MatchState} from "JS/enums/MatchState";

export default class Landingpage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {};
    }

    componentDidMount() {
        const scoresheet = Scoresheet.get.local();
        this.setState({scoresheet});
    }

    createMatch() {
        Store.removeLocalStorage('scoresheet');
        Tracking.click('enter', MatchState.SETUP);

        window.location.href = '/#/enter'; // TODO: use history.push('/enter')
    }

    render() {
        const {scoresheet} = this.state;
        const backgroundLive = {'backgroundImage': 'url(' + imgLive + ')'};
        const backgroundCreate = {'backgroundImage': 'url(' + imgCreate + ')'};

        return (
            <div id="landingpage">
                <Topbar/>

                <section className="landingpage">
                    <div className="container">
                        <article className="live-teaser" style={backgroundLive}>
                            <h1>Zusehen</h1>
                            <p>Verfolge live, laufende Begegnungen oder stöbere in vergangenen Spielen.</p>

                            <Link to="/history" className="btn-default" data-icon="live">
                                Spiele LIVE Verfolgen
                            </Link>
                            <div className="options">
                                {/* show possible live games here */}
                            </div>
                        </article>

                        <article className="create-teaser" style={backgroundCreate}>
                            <h1>Spielen</h1>
                            <p>Erstelle dein Ligaspiel. Oder starte ganz einfach ein Freundschaftsspiel.</p>

                            <button onClick={this.createMatch} className="btn-cta" data-icon="edit">
                                Neues Spiel starten!
                            </button>

                            <div className="options">
                                {(scoresheet) ? (
                                    <Link to="/enter" className="btn-default">
                                        Spiel weiterspielen
                                        <small>{moment(scoresheet.date.create).format('DD.MM.YYYY, HH:mm')}Uhr</small>
                                    </Link>
                                ) : null}
                            </div>
                        </article>
                    </div>
                </section>

                <Footerbar/>
            </div>
        );
    }

}
