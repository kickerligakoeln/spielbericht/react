import React from "react";
import {connect} from "react-redux";
import {showPageLoading, hidePageLoading} from "Redux/loadingStore";

import {Topbar} from "Dumb/Topbar/Topbar";
import {Footerbar} from "Dumb/Footerbar/Footerbar";
import LoginButton from "Dumb/Auth/LoginButton";
import LogoutButton from "Dumb/Auth/LogoutButton";
import Profile from "Dumb/Auth/Profile";

import "./account.scss";

class Account extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    render() {
        return (
            <div id="account">
                <Topbar/>

                <div className="account">
                    <div className="container">
                        <Profile />

                        <LoginButton />
                        <LogoutButton />
                    </div>
                </div>

                <Footerbar/>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    loadingStore: state.loadingStore
});

export default connect(mapStateToProps, {
    showPageLoading,
    hidePageLoading,
})(Account);
