import {createSlice} from "@reduxjs/toolkit";
import {NotificationType} from "JS/enums/NotificationType";

export const notificationStore = createSlice({
    name: 'notification',
    initialState: {
        show: false,
        type: null,
        message: null
    },
    reducers: {
        hideNotification: (state, action) => {
            state.show = false;
            state.type = null;
            state.message = null;
        },
        showNotification: (state, action) => {
            state.show = true;
            state.type = action.payload.type || NotificationType.HINT;
            state.message = action.payload.message || null;
        }
    }
});

export const {
    hideNotification,
    showNotification
} = notificationStore.actions;

export default notificationStore.reducer;