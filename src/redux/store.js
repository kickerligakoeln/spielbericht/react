import { configureStore } from '@reduxjs/toolkit'

import loadingStore from "Redux/loadingStore";
import scoresheetStore from "Redux/scoresheetStore";
import notificationStore from "Redux/notificationStore";

export const store = configureStore({
    reducer: {
        loadingStore,
        scoresheetStore,
        notificationStore
    },
})
