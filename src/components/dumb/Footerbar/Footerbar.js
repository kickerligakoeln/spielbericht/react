import React from "react";
import {Link} from "react-router-dom";

import pckgJson from '/package.json';
import './footerbar.scss';
import Brand from "Images/brand_schwarz.svg";

export function Footerbar() {

    return (
        <section className="footerbar">
            <div className="container">
                <div className="brand">
                    <Brand />
                </div>

                <ul>
                    <li>
                        <p>
                            {new Date().getFullYear()}, © <strong>Kölner Kickerliga</strong><br/>
                            Digitaler Spielberichtsbogen, v{pckgJson.version}
                        </p>
                    </li>
                    <li><a target="_blank" rel="noopener noreferrer" href="https://kickerligakoeln.de"
                           className="btn-link">www.kickerligakoeln.de</a></li>
                    <li>
                        <a target="_blank" rel="noopener noreferrer"
                           href="https://www.kickerligakoeln.de/impressum/"
                           className="btn-link">Impressum</a>&nbsp;-&nbsp;
                        <Link to={'./privacy/'} className="btn-link">Datenschutz</Link>
                    </li>
                </ul>
            </div>
        </section>
    );
}
