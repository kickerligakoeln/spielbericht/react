import React from "react";
import {useSelector} from "react-redux";

import "./trend.scss"

export function ScoresheetTrend() {
    const scoresheet = useSelector((state) => state.scoresheetStore.scoresheet);

    if (!scoresheet.matches) {
        return null;
    } else {
        return (
            <div className="scoresheet-trend">
                <ul data-label="Gast / Heim">
                    {scoresheet.matches.map((match, index) => (
                        <li key={index}
                            className={(scoresheet.winner && scoresheet.winner.match === (index + 1)) ? '-winner' : ''}>
                            <span data-label={Math.abs(match.score[0].home - match.score[0].guest)}
                                  data-trend={(match.score[0].home - match.score[0].guest)}></span>

                            <span data-label={Math.abs(match.score[1].home - match.score[1].guest)}
                                  data-trend={(match.score[1].home - match.score[1].guest)}></span>
                        </li>
                    ))}
                </ul>
            </div>
        );
    }
}
