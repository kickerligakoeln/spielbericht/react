import React, {useEffect} from "react";
import {useSelector} from "react-redux";
import {Circle} from "../Circle/Circle";
import find from "lodash.find"

import "./players.scss"

export function ScoresheetPlayers(props) {
    const scoresheet = useSelector((state) => state.scoresheetStore.scoresheet);
    const performances = useSelector((state) => state.scoresheetStore.playerPerformance);
    const team = props.team;

    const teamPerformances = find(performances, {_id: team});

    function tableRow(player, index) {
        let playerPerformance = null;

        if (teamPerformances) {
            playerPerformance = find(teamPerformances.data, (p) => {
                return p._id.player === player.name;
            });
        }

        return (
            <div className="table-row-players" key={index}>
                <div className="index">{index}.</div>
                <div className="player"><strong>{player.name}</strong></div>
                {(playerPerformance) ? (
                    <div className="performance">
                        <div className="goals">
                            {playerPerformance.goalsWin}&nbsp;:&nbsp;{playerPerformance.goalsLosse}
                        </div>
                        <div className="amount">
                            {playerPerformance.win}&nbsp;
                            <small>/&nbsp;{playerPerformance.total}</small>
                        </div>
                        <div className="circle">
                            <Circle value={(playerPerformance.win*100/playerPerformance.total)}/>
                        </div>
                    </div>
                ) : (
                    <div className="performance">
                        <div className="goals">-&nbsp;:&nbsp;-</div>
                        <div className="amount">-&nbsp;/&nbsp;-
                        </div>
                        <div className="circle">
                            <Circle value={null}/>
                        </div>
                    </div>
                )}
            </div>
        )
    }

    if (!scoresheet[`team_${team}`]) {
        return null;
    } else {
        return (
            <div className="scoresheet-players">
                <div className="table -tiles">
                    <div className="table-head-players">
                        <div className="index">#</div>
                        <div className="player">{scoresheet[`team_${team}`].name}</div>
                        <div className="performance">
                            <div className="goals">Tore</div>
                            <div className="amount">Sätze</div>
                            <div className="circle"></div>
                        </div>
                    </div>

                    {(scoresheet[`team_${team}`].player) ? (
                        scoresheet[`team_${team}`].player.map((player, index) => tableRow(player, index + 1))
                    ) : null}
                </div>
            </div>
        );

    }
}
