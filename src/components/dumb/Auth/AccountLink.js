import React from "react";
import { useAuth0 } from "@auth0/auth0-react";
import {Link} from "react-router-dom";
import User from "../../../js/utils/User";
import {UserRole} from "../../../js/enums/UserRole";

const AccountLink = (props) => {
    const { user, isAuthenticated } = useAuth0();

    const isLogin = (isAuthenticated) ? '-login' : '';
    const isAdmin = (User.hasRole(user, UserRole.ADMIN)) ? '-admin' : '';

    return (
        <Link className={`user ${isLogin} ${isAdmin}`}
                 data-icon={(isAuthenticated) ? 'user-loggedin' : 'user'}
                 to={"/account"}>{props.label}</Link>
    )
};

export default AccountLink;
