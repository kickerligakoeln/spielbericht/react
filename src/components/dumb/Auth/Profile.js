import React from "react";
import {useAuth0} from "@auth0/auth0-react";
import {UserRole} from "JS/enums/UserRole";
import User from "JS/utils/User";

import "./profile.scss";

const Profile = () => {
    const {user, isAuthenticated, isLoading} = useAuth0();

    if (isLoading) {
        return <div>Loading ...</div>;
    }

    return (
        isAuthenticated && (
            <div className="profile">
                <img src={user.picture} alt={user.name}/>

                <div className="input-group">
                    <label htmlFor="role">Role</label>
                    <input id="role" className="input-text"
                           type="text"
                           disabled
                           value={(User.hasRole(user, UserRole.ADMIN) ? 'Admin' : 'User')}/>
                </div>

                <div className="input-group">
                    <label htmlFor="user_id">ID</label>
                    <input id="user_id" className="input-text" type="text" disabled value={user.sub}/>
                </div>
                <div className="input-group">
                    <label htmlFor="name">Name</label>
                    <input id="name" className="input-text" type="text" disabled value={user.name}/>
                </div>
                <div className="input-group">
                    <label htmlFor="email">E-Mail</label>
                    <input id="email" className="input-text" type="text" disabled value={user.email}/>
                </div>

                {(User.hasRole(user, UserRole.ADMIN)) ? (
                    <pre>
                        {JSON.stringify(user, null, 2)}
                    </pre>
                ) : null}
            </div>
        )
    );
};

export default Profile;
