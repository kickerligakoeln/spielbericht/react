import React, {useState} from "react";
import {Link} from "react-router-dom";

import "./navigation.scss"
import AccountLink from "../Auth/AccountLink";

export function Navigation() {
    const [css, setCss] = useState('');
    const [currentState, setCurrentState] = useState(false);

    function toggleNavigation() {
        setCss((!currentState) ? '-in' : '');
        setCurrentState(!currentState);
    }

    return (
        <div>
            <button className={`nav-toggle ${css}`} onClick={toggleNavigation}>&nbsp;</button>

            <section className={`navigation -fix ${css}`}>
                <div className="container">

                    <ul>
                        <li><Link to={'/'} data-icon="home">Digitaler Spielberichtsbogen</Link></li>
                        <li><Link to={'/history'} data-icon="live">Live</Link></li>
                        <li><Link to={'/performance'} data-icon="graph-2">Performance</Link></li>
                        <li><a href="https://www.kickerligakoeln.de/spielbetrieb/downloads/" target="_blank"
                               rel="noopener noreferrer" data-icon="light">Spielregeln runterladen</a></li>
                        <li><a href="https://www.kickerligakoeln.de/" target="_blank" data-icon="earth"
                               rel="noopener noreferrer">Kölner Kickerliga</a></li>
                        <li>
                            <AccountLink label="Account" />
                        </li>
                    </ul>

                </div>
            </section>
        </div>
    );
}
