import React from "react";
import {connect} from "react-redux";

import {showPageLoading, hidePageLoading} from "Redux/loadingStore";
import './loading-fullsize.scss'

class LoadingFullsize extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    render() {
        const {pending, message} = this.props.loadingStore;

        return (
            <div className={`loading -fix ${(pending) ? '-in' : ''}`}>
                <div className="lds-heart">
                    <div></div>
                </div>
                <p>{message}</p>

                <div className="cred">{new Date().getFullYear()}, © <strong>Kölner Kickerliga</strong></div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    loadingStore: state.loadingStore
});

export default connect(mapStateToProps, {showPageLoading, hidePageLoading})(LoadingFullsize);
