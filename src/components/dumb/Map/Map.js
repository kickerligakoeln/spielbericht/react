import React from "react";
import {useSelector} from "react-redux";
import {LoadScript, GoogleMap, Marker} from "@react-google-maps/api";

import "./map.scss"

export function ScoresheetMap() {
    const scoresheet = useSelector((state) => state.scoresheetStore.scoresheet);

    if (!scoresheet || !scoresheet.location) {
        return null;
    } else {
        if(scoresheet.location.lat) {
            const center = {
                lat: scoresheet.location.lat,
                lng: scoresheet.location.lng
            };

            return (
                <div className="scoresheet-map">
                    <div className="container">

                        <LoadScript id="script-loader"
                                    googleMapsApiKey={process.env.GOOGLE_API_KEY}>
                            <GoogleMap
                                id="map"
                                zoom={15}
                                center={center}
                                coords={center}
                                options={{
                                    fullscreenControl: false,
                                    mapTypeControl: false,
                                    streetViewControl: false
                                }}>
                                <Marker
                                    position={center}
                                    label={scoresheet.location.title}
                                />
                            </GoogleMap>
                        </LoadScript>

                    </div>
                </div>
            );
        } else {
            return null;
        }
    }
}
