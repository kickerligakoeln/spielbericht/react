import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {Settings} from "JS/utils/Settings";
import {showNotification} from "Redux/notificationStore";

import "./share.scss"
import {NotificationType} from "../../../js/enums/NotificationType";

export function ScoresheetShare() {
    const dispatch = useDispatch();
    const scoresheet = useSelector((state) => state.scoresheetStore.scoresheet);
    
    let shareButton = {
        facebook: (text, url) => {
            const baseUrl = "https://www.facebook.com/dialog/share";
            return `${baseUrl}?app_id=${Settings.get('REACT_APP_FACEBOOK_ID')}&display=popup&href=${encodeURIComponent(url)}`;
        },
        twitter: (text, url) => {
            const baseUrl = "http://twitter.com/share";
            return baseUrl + "?text=" + encodeURIComponent(text) + "&url=" + encodeURIComponent(url);
        },
        messanger: (text, url) => {
            const baseUrl = "fb-messenger://share";
            return `${baseUrl}?link=${encodeURIComponent(url)}&app_id=${Settings.get('REACT_APP_FACEBOOK_ID')}`;
        },
        whatsapp: (text, url) => {
            const baseUrl = "whatsapp://send";
            return baseUrl + "?text=" + encodeURIComponent(text) + "&url=" + encodeURIComponent(url);
        },
        telegram: (text, url) => {
            const baseUrl = 'https://telegram.me/share';
            return baseUrl + '/url?url=' + encodeURIComponent(url) + '&text=' + encodeURIComponent(text);
        }
    };


    /**
     *
     * @param type
     */
    function share(type) {
        let config = {
            url: window.location.href,
            text: 'Kölner Kickerlga | Digitaler Spielberichtsbogen'
        };

        if (scoresheet) {
            config.url = `${window.location.origin}#/live/${scoresheet.matchid}`;
            config.text = `${scoresheet.team_home.name} vs ${scoresheet.team_guest.name}`;
        }

        window.open(
            shareButton[type](config.text, config.url),
            type,
            'width=575,height=400');
    }


    /**
     *
     */
    function copyLink() {
        // Create a dummy input to copy the string array inside it
        let linkElement = document.createElement("input");
        document.body.appendChild(linkElement);
        linkElement.setAttribute("id", "copy-link");
        document.getElementById("copy-link").value = window.location.href;

        linkElement.select();
        document.execCommand("copy");
        document.body.removeChild(linkElement);

        dispatch(showNotification({
            type: NotificationType.SUCCESS,
            message: 'Link copied!'
        }));
    }


    return (
        <div className="scoresheet-share">
            <div className="container">
                <ul>
                    {/*<li>*/}
                    {/*    <button onClick={() => share('messanger')} data-icon="messanger"></button>*/}
                    {/*</li>*/}
                    <li>
                        <button onClick={() => share('whatsapp')} data-icon="whatsapp"></button>
                    </li>
                    <li>
                        <button onClick={() => share('telegram')} data-icon="telegram"></button>
                    </li>
                    <li>
                        <button onClick={() => copyLink()} data-icon="link"></button>
                    </li>
                </ul>
            </div>
        </div>
    );
}