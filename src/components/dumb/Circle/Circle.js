import React from "react";

import "./circle.scss"

export function Circle(props) {
    const increment = 180 / 100;
    const fill = {
        transform: `rotate(${increment * props.value}deg)`
    };
    const fillFix = {
        transform: `rotate(${increment * props.value * 2}deg)`
    };

    return (
        <div className="radial-progress" data-progress={props.value}>
            <div className="circle">
                <div className="mask full" style={fill}>
                    <div className="fill" style={fill}></div>
                </div>
                <div className="mask half">
                    <div className="fill" style={fill}></div>
                    <div className="fill fix" style={fillFix}></div>
                </div>
            </div>
            <div className="inset"></div>
        </div>
    );
}
