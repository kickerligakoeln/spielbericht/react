import React, {useState} from "react";
import {useSelector} from "react-redux";

import {ScoresheetScoreboard} from "Dumb/Scoreboard/Scoreboard";

import "./title.scss"

export function ScoresheetTitle() {
    const scoresheet = useSelector((state) => state.scoresheetStore.scoresheet);
    const [scroll, setScroll] = useState('');
    const handleScroll = event => {
        if (window.pageYOffset > 0) {
            setScroll('-scroll');
        } else {
            setScroll('');
        }
    }

    window.addEventListener('scroll', handleScroll.bind(this));

    if (scoresheet.team_home && scoresheet.team_guest) {
        return (
            <div className={`scoresheet-title -sticky ${scroll}`}>
                <div className="container">
                    <div className="team">{scoresheet.team_home.name}</div>
                    <ScoresheetScoreboard />
                    <div className="team">{scoresheet.team_guest.name}</div>
                </div>
            </div>
        )
    } else {
        return null;
    }
}
