import React from "react";
import {useSelector} from "react-redux";
import Player from "JS/utils/Player";

import "./draw.scss"

export function ScoresheetDraw() {
    const scoresheet = useSelector((state) => state.scoresheetStore.scoresheet);

    if (!scoresheet.matches) {
        return null;
    } else {
        return (
            <div className="scoresheet-draw">

                <div className="table -striped">
                    <div className="table-head-draw">
                        <div className="index">#</div>
                        <div className="team-home">Heim</div>
                        <div className="divider"></div>
                        <div className="team-guest">Gast</div>
                        <div className="score">
                            <div className="score-home">Satz 1</div>
                            <div className="score-guest">Satz 2</div>
                        </div>
                    </div>

                    {scoresheet.matches.map((r, index) => (
                        <div className="table-row-draw" key={index}>
                            <div className="index">{index + 1}.</div>
                            <div className="team-home">
                                {Player.shortName(r.player.home_a)}<br/>
                                {Player.shortName(r.player.home_b)}
                            </div>
                            <div className="divider">vs</div>
                            <div className="team-guest">
                                {Player.shortName(r.player.guest_a)}<br/>
                                {Player.shortName(r.player.guest_b)}
                            </div>
                            <div className="score">
                                <div className="score-home">
                                    {r.score[0].home}:{r.score[0].guest}
                                </div>
                                <div className="score-guest">
                                    {r.score[1].home}:{r.score[1].guest}
                                </div>
                            </div>
                        </div>
                    ))}
                </div>

            </div>
        );
    }
}
