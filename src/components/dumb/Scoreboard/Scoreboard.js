import React from "react";
import {useSelector} from "react-redux";

import "./scoreboard.scss"

export function ScoresheetScoreboard() {
    const scoresheet = useSelector((state) => state.scoresheetStore.scoresheet);

    if (scoresheet.team_home && scoresheet.team_guest) {
        return (
            <div className="scoreboard-wrapper">
                <div className="setcount text-center">
                    <div className="scoreboard">
                        <span className="setcount_home">{scoresheet.team_home.set}</span>
                        <span className="count_eq">:</span>
                        <span className="setcount_guest">{scoresheet.team_guest.set}</span>
                    </div>
                </div>
                <div className="goalcount text-center">
                    <div className="scoreboard">
                        <span className="goalcount_home">{scoresheet.team_home.goals}</span>
                        <span className="count_eq">:</span>
                        <span className="goalcount_guest">{scoresheet.team_guest.goals}</span>
                    </div>
                </div>
            </div>
        );
    } else {
        return null;
    }
}
