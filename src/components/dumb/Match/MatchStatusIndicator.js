import React from "react";
import {VisibleMatchStatus} from "JS/enums/MatchState";
import PlayIcon from "Images/icons/play-circle.svg";
import WarningIcon from "Images/icons/exclamation-circle.svg";

export function MatchStatusIndicator(props) {

    return (
        <div className={`match-status -${props.status}`}>
            {(props.status === VisibleMatchStatus.LIVE) ? <PlayIcon/> : null}
            {/*{(props.status === VisibleMatchStatus.DONE) ? <CheckIcon/> : null}*/}
            {(props.status === VisibleMatchStatus.WARNING) ? <WarningIcon/> : null}
        </div>
    )
}