import React from 'react';
import {Link} from "react-router-dom";
import moment from "moment";

import {GameType} from "JS/enums/GameType";
import League from "JS/utils/League";
import Factory from "JS/utils/Factory";

import './match.scss';
import {MatchStatusIndicator} from "Dumb/Match/MatchStatusIndicator";
import {useSelector} from "react-redux";

export const Match = (props) => {
    const scoresheet = useSelector((state) => state.scoresheetStore.scoresheet);

    /**
     *
     * @param m
     * @returns {*}
     */
    function htmlAvatar(m) {
        const leagueName = (m.gameType === GameType.GAMEDAY) ? new League().shortName(m.league) : '';

        return (
            <div className={`avatar -${m.gameType}`}>
                {leagueName}
            </div>
        );
    }


    return (
        <ul className="match-list">
            {props.matches.map(match => (

                (match.matchid !== scoresheet.matchid) ? (
                    <li className="match-teaser"
                        data-id={match.matchid}
                        key={match.matchid}>
                        {htmlAvatar(match)}

                        <MatchStatusIndicator status={Factory.get.status(match)}/>

                        <div className="match">
                            <Link to={`/live/${match.matchid}`} className="">
                                <small className="date">{moment(match.date.create).format('L')}</small><br/>
                                <strong>{match.team_home.name}</strong> vs<br/>
                                <strong>{match.team_guest.name}</strong>
                            </Link>
                        </div>

                        <div className="result">
                            {(match.team_home.set) ? match.team_home.set : 0} : {(match.team_guest.set) ? match.team_guest.set : 0}
                        </div>
                    </li>
                ) : null

            ))}
        </ul>
    );
}
