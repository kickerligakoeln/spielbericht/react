import React from "react";
import {connect} from "react-redux";
import {showPageLoading, hidePageLoading} from "Redux/loadingStore";
import {buildScoresheetStore} from "Redux/scoresheetStore";
import {showNotification} from "Redux/notificationStore";

import Kickerliga from "JS/rest/Kickerliga";
import {GameType} from "JS/enums/GameType";
import {NotificationType} from "JS/enums/NotificationType";
import find from "lodash.find";

class FormSetupGameday extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoaded: false
        }

        this.chooseLeague = this.chooseLeague.bind(this);
        this.chooseMatch = this.chooseMatch.bind(this);
    }


    componentDidMount() {
        this.props.showPageLoading('load active leagues');

        Kickerliga.activeLeagues().then(activeLeagues => {
            this.setState({
                isLoaded: true,
                activeLeagues
            });

            this.props.hidePageLoading();
        });
    }


    /**
     * render list of leagues
     * @returns {JSX.Element}
     */
    renderLeagues() {
        const {activeLeagues} = this.state;

        if (activeLeagues) {
            return (
                <div className="row input-group">
                    <label htmlFor="league">Liga*</label>
                    <select id="league" className="input-select"
                            name="league"
                            onChange={this.chooseLeague}
                            defaultValue="0">
                        <option value="0" disabled>Liga wählen</option>
                        {activeLeagues.map(league => (
                            <option value={league._embedded.currentseason.currentGameDay}
                                    key={league.id}>{league.name}</option>
                        ))}
                    </select>
                </div>
            )
        }
    }

    /**
     * choose one league
     * @param event
     */
    chooseLeague(event) {
        this.props.showPageLoading('load upcoming matches');
        this.setState({upcomingMatches: null});

        const currentGameDayId = event.target.value;
        const chosenLeague = find(this.state.activeLeagues, (l) => {
            return l._embedded.currentseason.currentGameDay === parseFloat(currentGameDayId);
        });

        Kickerliga.upcomingMatches(currentGameDayId).then(upcomingMatches => {
            if (upcomingMatches) {
                this.props.buildScoresheetStore({
                    build: 'match',
                    data: {
                        gameType: GameType.GAMEDAY,
                        leagueCode: chosenLeague.code,
                        leagueName: chosenLeague.name,
                        currentSeason: chosenLeague.currentSeason,
                        gamedayId: upcomingMatches[0].gameDayId,
                        gamedayNumber: upcomingMatches[0]._embedded.gameDay.number
                    }
                });

                this.setState({
                    currentGameDayNumber: upcomingMatches[0]._embedded.gameDay.number,
                    upcomingMatches
                });
            } else {
                this.props.showNotification({
                    type: NotificationType.ERROR,
                    message: 'There are no games available'
                });
            }

            this.props.hidePageLoading();
        });
    }

    /**
     * render list of upcoming matches
     * @returns {JSX.Element}
     */
    renderMatches() {
        const {upcomingMatches} = this.state;

        return (
            <div>
                <div className="row input-group">
                    <label htmlFor="gameday">Spieltag</label>
                    <input id="gameday" type="text" className="input-text"
                           value={(this.state.currentGameDayNumber) ? this.state.currentGameDayNumber : '-'}
                           disabled/>
                </div>

                <div className="row input-group">
                    <label htmlFor="matches">Begegnungen*</label>
                    <select id="matches" className="input-select"
                            name="matches"
                            onChange={this.chooseMatch}
                            disabled={(!upcomingMatches) ? 'disabled' : ''}
                            defaultValue="0">
                        <option value="0" disabled>Wähle eine Begegnung</option>
                        {(upcomingMatches) ? upcomingMatches.map(match => (
                            <option value={match.id}
                                    key={match.id}>{match._embedded.homeTeam.name} vs {match._embedded.awayTeam.name}</option>
                        )) : null}
                    </select>
                </div>
            </div>
        )
    }

    /**
     *
     * @param event
     */
    chooseMatch(event) {
        this.props.showPageLoading('load team data');
        this.setState({matchId: null});
        const matchId = event.target.value;
        const chosenMatch = find(this.state.upcomingMatches, (m) => {
            return m.id === parseFloat(matchId);
        });

        let teamPromises = [
            Kickerliga.team(chosenMatch._embedded.homeTeam.id),
            Kickerliga.team(chosenMatch._embedded.awayTeam.id),
        ];

        Promise.all(teamPromises).then(response => {
            const homeTeam = response[0];
            const awayTeam = response[1];

            this.props.buildScoresheetStore({build: 'id', data: chosenMatch.id});
            this.props.buildScoresheetStore({build: 'team', data: {type: 'team_home', team: homeTeam}});
            this.props.buildScoresheetStore({build: 'team', data: {type: 'team_guest', team: awayTeam}});
            this.props.buildScoresheetStore({build: 'mail', data: {type: 'team_home', team: homeTeam.properties}});
            this.props.buildScoresheetStore({build: 'mail', data: {type: 'team_guest', team: awayTeam.properties}});

            this.setState({
                matchId: chosenMatch.id,
            });

            this.props.hidePageLoading();
        });
    }


    render() {
        if (!this.state.isLoaded) {
            return (
                <div className="loading-inline">
                    <div className="spinner -small">
                        <div className="rect1"></div>
                        <div className="rect2"></div>
                        <div className="rect3"></div>
                        <div className="rect4"></div>
                        <div className="rect5"></div>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="container">
                    {this.renderLeagues()}
                    {this.renderMatches()}

                    <div className="row input-group">
                        <label htmlFor="mail_home">E-Mail Heimmannschaft</label>
                        <input id="mail_home" type="email" className="input-text"
                               placeholder="Wird automatisch gesetzt"
                               disabled/>
                    </div>

                    <div className="row input-group">
                        <label htmlFor="mail_guest">E-Mail Gastmannschaft</label>
                        <input id="mail_guest" type="email" className="input-text"
                               placeholder="Wird automatisch gesetzt"
                               disabled/>
                    </div>
                </div>
            );
        }
    }
}

const mapStateToProps = state => ({
    loadingStore: state.loadingStore,
    scoresheetStore: state.scoresheetStore,
    notificationStore: state.notificationStore
});

export default connect(mapStateToProps, {showPageLoading, hidePageLoading, buildScoresheetStore, showNotification})(FormSetupGameday);
