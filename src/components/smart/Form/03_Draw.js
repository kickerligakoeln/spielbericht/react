import React from "react";
import {connect} from "react-redux";
import {buildScoresheetStore} from "Redux/scoresheetStore";
import {hidePageLoading, showPageLoading} from "Redux/loadingStore";

import {DrawingType} from "JS/enums/DrawingType";
import {MatchState} from "JS/enums/MatchState";
import {KKL} from "JS/setup/KKL";
import Player from "JS/utils/Player";
import Scoresheet from "JS/rest/Scoresheet";
import Tracking from "JS/rest/Tracking";

import "./draw-matches.scss";

class FormDraw extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoaded: false
        }

        this.customDraw = this.customDraw.bind(this);
        this.autoDraw = this.autoDraw.bind(this);
        this.submitForm = this.submitForm.bind(this);
    }

    componentDidMount() {
        this.setState({
            drawingType: this.props.scoresheetStore.scoresheet.drawingType || null,
            submit: false,
            isLoaded: true
        });
    }


    componentWillUnmount() {
        window.removeEventListener('click', this.customDraw, false);
        window.removeEventListener('click', this.autoDraw, false);
        window.removeEventListener('click', this.submitForm, false);
    }


    /**
     * show auto draw html
     */
    autoDraw(event) {
        event.preventDefault();
        this.props.showPageLoading("Take a Beer! We throw the dice.")

        setTimeout(() => {
            this.props.buildScoresheetStore({build: 'draw', data: null});
            this.setState({
                drawingType: DrawingType.AUTO,
                submit: true
            });

            this.props.hidePageLoading();
        }, 2000);
    }

    /**
     * render auto drawed matches
     */
    htmlAutoDraw() {
        let {scoresheet} = this.props.scoresheetStore;
        let matches = [];

        if (scoresheet.matches.length) {
            scoresheet.matches.map((r, index) => (
                matches.push(
                    <div className="table-row-draw" key={index}>
                        <div className="index">{index + 1}.</div>
                        <div className="team-home">
                            {Player.shortName(r.player.home_a)}<br/>
                            {Player.shortName(r.player.home_b)}
                        </div>
                        <div className="divider">vs</div>
                        <div className="team-guest">
                            {Player.shortName(r.player.guest_a)}<br/>
                            {Player.shortName(r.player.guest_b)}
                        </div>
                    </div>
                )
            ));
        }

        return (<div>{matches}</div>)
    }


    /**
     * show custom draw html
     */
    customDraw(event) {
        event.preventDefault();
        this.setState({
            drawingType: DrawingType.CUSTOM,
            // scoresheet: Draw.custom.init(this.state.scoresheet), // TODO
            submit: false
        });
    }

    /**
     * render select boxes for custom draw
     * @returns {JSX.Element}
     */
    htmlCustomDraw() {
        let {scoresheet} = this.props.scoresheetStore;
        let row = [];

        for (let i = 0; i < KKL.AMOUNT_MATCHES; i++) {
            row.push(
                <div className="table-row-draw" key={'match-' + i}>
                    <div className="index">{i + 1}.</div>
                    <div className="team-home">
                        <select className="input-select"
                                id="home_a"
                                name="home_a"
                                onChange={(e) => this.choosePlayer(e, i)}
                                defaultValue="0">
                            <option value="0" disabled>Spieler 1</option>
                            {(scoresheet.team_home.player.map((p) => (
                                <option value={p.name} key={p.name}>{p.name}</option>
                            )))}
                        </select>

                        <select className="input-select"
                                id="home_b"
                                name="home_b"
                                onChange={(e) => this.choosePlayer(e, i)}
                                defaultValue="0">
                            <option value="0" disabled>Spieler 2</option>
                            {(scoresheet.team_home.player.map((p) => (
                                <option value={p.name} key={p.name}>{p.name}</option>
                            )))}
                        </select>
                    </div>
                    <div className="divider">vs</div>
                    <div className="team-guest">
                        <select className="input-select"
                                id="guest_a"
                                name="guest_a"
                                onChange={(e) => this.choosePlayer(e, i)}
                                defaultValue="0">
                            <option value="0" disabled>Spieler 1</option>
                            {(scoresheet.team_guest.player.map((p) => (
                                <option value={p.name} key={p.name}>{p.name}</option>
                            )))}
                        </select>

                        <select className="input-select"
                                id="guest_b"
                                name="guest_b"
                                onChange={(e) => this.choosePlayer(e, i)}
                                defaultValue="0">
                            <option value="0" disabled>Spieler 2</option>
                            {(scoresheet.team_guest.player.map((p) => (
                                <option value={p.name} key={p.name}>{p.name}</option>
                            )))}
                        </select>
                    </div>
                </div>
            )
        }

        return (<div>{row}</div>);
    }

    /**
     *
     * @param event
     * @param row
     */
    choosePlayer(event, row) {
        this.props.buildScoresheetStore({
            build: 'draw', data: {
                match: row,
                teamPos: event.target.name,
                player: event.target.value
            }
        })

        this.validateScoresheet();
    }


    /**
     *
     * @param event
     */
    submitForm(event) {
        event.preventDefault();

        Scoresheet.set.match(this.props.scoresheetStore.scoresheet)
            .then(scoresheet => {
                this.props.buildScoresheetStore({
                    build: 'state',
                    data: MatchState.IN_PROGRESS
                });
                Tracking.click('enter', MatchState.IN_PROGRESS);
            });
    }

    render() {
        const {scoresheet} = this.props.scoresheetStore;

        if (!this.state.isLoaded) {
            return null;
        } else {
            return (
                <form className="create-draw-matches" onSubmit={this.submitForm} noValidate>
                    <div className="section-head">
                        <div className="container">
                            <h1>Deine Losung</h1>
                            <p>Lose deine Spielbegegnungen aus.</p>
                        </div>
                    </div>

                    <div className="section-submit">
                        <div className="container">
                            <div className="row">
                                <div></div>
                                <div data-grid="2">
                                    <span className="btn-default -disabled" data-icon="edit">
                                        Manuell losen
                                    </span>
                                    <button className="btn-cta" data-icon="draw" onClick={this.autoDraw}>
                                        Automatisch losen
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="section-form">
                        <div className="container">
                            <div className="row">
                                <div></div>
                                <div className="table -striped -small">

                                    <div className="table-head-draw">
                                        <div className="index">#</div>
                                        <div className="team-home">Heim</div>
                                        <div className="divider"></div>
                                        <div className="team-guest">Gast</div>
                                    </div>

                                    {(!this.state.drawingType) ? (
                                        <div className="table-empty">
                                            No entries yet
                                        </div>
                                    ) : null}

                                    {(this.state.drawingType === DrawingType.AUTO) ? this.htmlAutoDraw() : null}
                                    {(this.state.drawingType === DrawingType.CUSTOM) ? this.htmlCustomDraw() : null}

                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="section-submit">
                        <div className="container">
                            <button type="submit" className="btn-cta" data-icon="heart"
                                    disabled={!(scoresheet.matches)}>
                                Match starten
                            </button>
                        </div>
                    </div>
                </form>
            )
        }
    }

}

const mapStateToProps = state => ({
    loadingStore: state.loadingStore,
    scoresheetStore: state.scoresheetStore
});

export default connect(mapStateToProps, {
    showPageLoading,
    hidePageLoading,
    buildScoresheetStore
})(FormDraw);
