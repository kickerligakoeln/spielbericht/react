import React, {useState} from "react";
import { useTranslation } from 'react-i18next';

import "./tottle-gametype.scss";

const ToggleGameType = ({emitEvent, types}) => {

    const { t, i18n } = useTranslation();
    const [currentGameType, setGameType] = useState(types[0])

    /**
     * toggle game type
     * @param newtype
     */
    function toggleGameType(newtype) {
        setGameType(newtype);
        emitEvent(newtype);
    }

    return (
        <div className="form-group gametype">
            {types.map(type => (
                <div className="input-group" key={type}>
                    <input type="radio" id={type} className="input-radio"
                           name="gameType"
                           value={type}
                           checked={currentGameType === type}
                           onChange={() => toggleGameType(type)}/>
                    <label htmlFor={type}>{t('gametype.' + type)}</label>
                </div>
            ))}
        </div>
    );
}

export default ToggleGameType;