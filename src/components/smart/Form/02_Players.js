import React from "react";
import {connect} from "react-redux";

import remove from "lodash.remove";
import {buildScoresheetStore} from "Redux/scoresheetStore";
import {hidePageLoading, showPageLoading} from "Redux/loadingStore";
import {MatchState} from "JS/enums/MatchState";
import {KKL} from "JS/setup/KKL";
import Scoresheet from "JS/rest/Scoresheet";
import Tracking from "JS/rest/Tracking";

import "./setup-players.scss";

class FormPlayers extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoaded: false
        }

        this.addPlayer = this.addPlayer.bind(this);
        this.handlePlayerName = this.handlePlayerName.bind(this);
        this.submitForm = this.submitForm.bind(this);
    }

    componentDidMount() {
        this.setState({
            isLoaded: true,
            team_home_name: '',
            team_home: this.props.scoresheetStore.scoresheet.team_home.player || [],
            team_guest_name: '',
            team_guest: this.props.scoresheetStore.scoresheet.team_guest.player || [],
        })
    }

    handlePlayerName = (event) => {
        const team = event.target.name;
        const name = event.target.value;
        this.setState({[team + '_name']: name});
    };


    addPlayer = (event, team) => {
        event.preventDefault();
        const playerName = this.state[team + '_name'];
        let players = [...this.state[team]];

        players.push({name: playerName, score: null});
        this.setState({
            [team]: players,
            [team + '_name']: ''
        });

        this.props.buildScoresheetStore({build: 'player', data: {team, players}});
    }


    removePlayer = (event, team, playerName) => {
        event.preventDefault();
        let players = [...this.state[team]];

        remove(players, (p) => {
            return p.name === playerName;
        });

        this.setState({[team]: players});

        this.props.buildScoresheetStore({build: 'player', data: {team, players}});
    };


    submitForm = () => {

        Scoresheet.set.match(this.props.scoresheetStore.scoresheet)
            .then(scoresheet => {
                    this.props.buildScoresheetStore({build: 'state', data: MatchState.DRAW_MATCHES});
                    Tracking.click('enter', MatchState.DRAW_MATCHES);
                }
            );
    }


    render() {
        const {isLoaded} = this.state;
        const {scoresheet} = this.props.scoresheetStore;
        let minPlayerReached = false;

        if (this.state.team_home && this.state.team_guest) {
            minPlayerReached = !(this.state.team_home.length >= KKL.MIN_PLAYERS && this.state.team_guest.length >= KKL.MIN_PLAYERS)
        }

        if (!isLoaded) {
            return null;
        } else {
            return (
                <div className="create-setup-players">
                    <div className="section-head">
                        <div className="container">
                            <h1>Spieler eintragen</h1>
                            <p>Trage die Namen der Mitspieler ein. Es ist alles erlaubt …</p>
                        </div>
                    </div>

                    {['team_home', 'team_guest'].map(team => (
                        <div key={team}>
                            <div className="section-form">
                                <div className="container">
                                    <div className="row -sticky">
                                        <div></div>
                                        <h2>{scoresheet[team].name}</h2>
                                    </div>

                                    <form className="row input-group" onSubmit={(e) => this.addPlayer(e, team)}>
                                        <label htmlFor={`${team}_player`}>Spieler hinzufügen</label>
                                        <input type="text" id={`${team}_player`}
                                               className="input-text"
                                               name={team}
                                               value={this.state[team + '_name']}
                                               placeholder="Spielername"
                                               onChange={this.handlePlayerName}/>
                                        <button type="submit"
                                                onClick={(e) => this.addPlayer(e, team)}
                                                disabled={this.state[team].length === KKL.MAX_PLAYERS}
                                                data-icon="add-user"></button>
                                    </form>

                                    {(scoresheet[team].player) ?
                                        scoresheet[team].player.map((p, index) => (
                                            <div className="row input-group -small" key={p.name}>
                                                <label>{index + 1}. Spieler</label>
                                                <input type="text" className="input-text" defaultValue={p.name}
                                                       disabled/>
                                                <button onClick={(e) => this.removePlayer(e, team, p.name)}
                                                        data-icon="close"></button>
                                            </div>
                                        ))
                                        : null}
                                </div>
                            </div>
                        </div>
                    ))}

                    <div className="section-submit">
                        <div className="container">
                            <div></div>
                            <div>
                                <button type="submit"
                                        className="btn-cta"
                                        data-icon="losen"
                                        disabled={minPlayerReached}
                                        onClick={() => this.submitForm()}>
                                    Begegnung losen
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
    }
}

const mapStateToProps = state => ({
    loadingStore: state.loadingStore,
    scoresheetStore: state.scoresheetStore
});

export default connect(mapStateToProps, {
    showPageLoading,
    hidePageLoading,
    buildScoresheetStore
})(FormPlayers);
