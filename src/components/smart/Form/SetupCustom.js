import React from "react";
import {connect} from "react-redux";
import {buildScoresheetStore} from "Redux/scoresheetStore";

import {GameType} from "JS/enums/GameType";

class FormSetupCustom extends React.Component {

    constructor(props) {
        super(props);

        this.updateMail = this.updateMail.bind(this);
        this.updateTeam = this.updateTeam.bind(this);
    }


    componentDidMount() {
        this.props.buildScoresheetStore({build: 'id', data: new Date().getTime()});
        this.props.buildScoresheetStore({build: 'match', data: {gameType: GameType.CUSTOM}});
    }


    /**
     *
     * @param event
     */
    updateTeam(event) {
        const type = event.target.name;
        const name = event.target.value;

        this.props.buildScoresheetStore({build: 'team', data: {type, team: {name}}});
    }


    /**
     *
     * @param event
     */
    updateMail(event) {
        const type = event.target.name;
        const captainEmail = event.target.value;

        this.props.buildScoresheetStore({build: 'mail', data: {type, team: {captainEmail}}});
    }


    render() {
        return (
            <div className="container">

                <div className="row input-group">
                    <label htmlFor="team_home">Heimmannschaft</label>
                    <input id="team_home" type="text" className="input-text"
                           placeholder="Teamname"
                           name="team_home"
                           onChange={this.updateTeam}/>
                </div>

                <div className="row input-group">
                    <label htmlFor="team_guest">Gastmannschaft</label>
                    <input id="team_guest" type="text" className="input-text"
                           placeholder="Teamname"
                           name="team_guest"
                           onChange={this.updateTeam}/>
                </div>

                <div className="row input-group">
                    <label htmlFor="mail_home">E-Mail Heimmannschaft</label>
                    <input id="mail_home" type="email" className="input-text"
                           placeholder="Heimkapitän"
                           name="team_home"
                           onChange={this.updateMail}/>
                </div>

                <div className="row input-group">
                    <label htmlFor="mail_guest">E-Mail Gastmannschaft</label>
                    <input id="mail_guest" type="email" className="input-text"
                           placeholder="Gastkapitän"
                           name="team_guest"
                           onChange={this.updateMail}/>
                </div>

            </div>
        )
    }
}

const mapStateToProps = state => ({
    scoresheetStore: state.scoresheetStore
});

export default connect(mapStateToProps, {buildScoresheetStore})(FormSetupCustom);
