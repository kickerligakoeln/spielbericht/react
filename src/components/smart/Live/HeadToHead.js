import React, {useEffect, useState} from "react";
import {Circle} from "Dumb/Circle/Circle";
import Performance from "JS/rest/Performance";

import DrawIcon from "Images/icons/fist-raised.svg";

import "./head-to-head.scss";

const HeadToHead = ({matchid, team_home, team_guest}) => {
    const [performance, setPerformance] = useState(null);
    const [promise, setPromise] = useState(true);

    useEffect(() => {
        setPromise(false);
        setPerformance(null);

        Performance.headToHead(matchid).then(performance => {

            performance['average'] = {
                avgWinHome: performance.total.totalWinHome * 100 / performance.total.totalGames,
                avgSetHome: performance.total.totalSetHome * 100 / (performance.total.totalSetHome + performance.total.totalSetGuest),
                avgSetGuest: performance.total.totalSetGuest * 100 / (performance.total.totalSetHome + performance.total.totalSetGuest),
                avgGoalsHome: performance.total.totalGoalsHome * 100 / (performance.total.totalGoalsHome + performance.total.totalGoalsGuest),
                avgGoalsGuest: performance.total.totalGoalsGuest * 100 / (performance.total.totalGoalsHome + performance.total.totalGoalsGuest),
                avgPlayerHome: performance.total.totalPlayerHome * 100 / (performance.total.totalPlayerHome + performance.total.totalPlayerGuest),
                avgPlayerGuest: performance.total.totalPlayerGuest * 100 / (performance.total.totalPlayerHome + performance.total.totalPlayerGuest)
            }

            setPerformance(performance);
            setPromise(true);
        });

    }, [matchid])

    return (
        <div className="head-to-head">
            {(performance && promise) ? (
                <div>
                    <div className="general">
                        <div>
                            <strong>Gesamt</strong><br/>
                            {performance.total.totalGames} Spielbericht(e)
                        </div>
                        <div><Circle value={performance.average.avgWinHome}/></div>
                    </div>

                    <div className="wins">
                        <div><strong>{team_home}</strong><br/>
                            <div className="count">{performance.total.totalWinHome}</div>
                        </div>
                        <div className="draw"><DrawIcon className="icon -draw" /><br/>
                            <div className="count">{performance.total.totalDraw}</div>
                        </div>
                        <div><strong>{team_guest}</strong><br/>
                            <div className="count">{performance.total.totalWinGuest}</div>
                        </div>
                    </div>

                    <div className="chart-distribution">

                        <div className="content">
                            <div className={`home ${(performance.total.totalSetHome > performance.total.totalSetGuest) ? '-active' : ''}`}>
                                <span style={{width: performance.average.avgSetHome + '%'}}>
                                    {performance.total.totalSetHome}
                                </span>
                            </div>
                            <div className="label"><strong>Sätze</strong></div>
                            <div className={`guest ${(performance.total.totalSetGuest > performance.total.totalSetHome) ? '-active' : ''}`}>
                                <span style={{width: performance.average.avgSetGuest + '%'}}>
                                    {performance.total.totalSetGuest}
                                </span>
                            </div>
                        </div>

                        <div className="content">
                            <div className={`home ${(performance.total.totalGoalsHome > performance.total.totalGoalsGuest) ? '-active' : ''}`}>
                                <span style={{width: performance.average.avgGoalsHome + '%'}}>
                                    {performance.total.totalGoalsHome}
                                </span>
                            </div>
                            <div className="label"><strong>Tore</strong></div>
                            <div className={`guest ${(performance.total.totalGoalsGuest > performance.total.totalGoalsHome) ? '-active' : ''}`}>
                                <span style={{width: performance.average.avgGoalsGuest + '%'}}>
                                    {performance.total.totalGoalsGuest}
                                </span>
                            </div>
                        </div>

                        <div className="content">
                            <div className={`home ${(performance.total.totalPlayerHome > performance.total.totalPlayerGuest) ? '-active' : ''}`}>
                                <span style={{width: performance.average.avgPlayerHome + '%'}}>
                                    {((performance.total.totalPlayerHome) / performance.total.totalGames).toFixed(2)}
                                </span>
                            </div>
                            <div className="label"><strong>Spieler</strong></div>
                            <div className={`guest ${(performance.total.totalPlayerGuest > performance.total.totalPlayerHome) ? '-active' : ''}`}>
                                <span style={{width: performance.average.avgPlayerGuest + '%'}}>
                                    {((performance.total.totalPlayerGuest) / performance.total.totalGames).toFixed(2)}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            ) : (
                <div className="fallback">
                    <p>Es liegen leider liegen noch keine Daten vor.</p>
                </div>
            )}
        </div>
    );
}

export default HeadToHead;