import React, { useEffect, useState } from "react";
import { AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer } from 'recharts';
import moment from "moment";

import Performance from "JS/rest/Performance";
import LoadingSpinner from "Dumb/Loading/Spinner";

const StatsFavoriteWeekDay = ({ season, gametype }) => {
    const [drawings, setDrawings] = useState([]);
    const [promise, setPromise] = useState(true);
    const COLORS = ['#EB432D', '#F4AA24', '#4a828b', '#151515'];

    const renderXAxis = ({ _id }) => {
        return moment(_id, 'd').format('dd');
    }

    useEffect(() => {
        setPromise(false);
        setDrawings(null);

        Performance.favoriteWeekday(season, gametype).then(resp => {
            setDrawings(resp);
            setPromise(true);
        })
    }, [season, gametype]);

    return (
        <article className="stats-tile">
            <h1>Wochentag</h1>
            <div className="chart">
                {(drawings && promise) ? (
                    <ResponsiveContainer width="100%" height="100%">
                        <AreaChart
                            width={500}
                            height={400}
                            data={drawings}
                            margin={{
                                top: 10,
                                right: 15,
                                left: 0,
                                bottom: 0,
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey={renderXAxis} />
                            <YAxis dataKey="total" width={25} />
                            <Tooltip />
                            <Area type="monotone" dataKey="total" stroke={COLORS[0]} fill={COLORS[1]} />
                        </AreaChart>
                    </ResponsiveContainer>
                ) : (
                    <LoadingSpinner />
                )}
            </div>
        </article>
    )

}

export default StatsFavoriteWeekDay;