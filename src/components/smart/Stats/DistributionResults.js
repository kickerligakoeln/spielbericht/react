import React, {useEffect, useState} from "react";
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

import LoadingSpinner from "Dumb/Loading/Spinner";
import Performance from "JS/rest/Performance";

const StatsDistributionResults = ({season, gametype}) => {
    const [drawings, setDrawings] = useState([]);
    const [promise, setPromise] = useState(true);
    const COLORS = ['#EB432D', '#F4AA24', '#4a828b', '#151515'];

    useEffect(() => {
        setPromise(false);
        setDrawings(null);

        Performance.distributionResults(season, gametype)
            .then(response => {
                setDrawings(response);
                setPromise(true);
            })
    }, [season, gametype]);

    return (
        <article className="stats-tile">
            <h1>Ergebnisse</h1>
            <div className="chart">
                {(drawings && promise) ? (
                    <ResponsiveContainer width="100%" height="100%">
                        <BarChart
                            width={500}
                            height={400}
                            data={drawings}
                            margin={{
                                top: 10,
                                right: 15,
                                left: 0,
                                bottom: 0,
                            }}>
                            <CartesianGrid strokeDasharray="3 3" />
                            <YAxis width={25} />
                            <XAxis
                                dataKey="_id"
                                tick={{fontSize: 11}}
                                tickFormatter={(label) => `${label}:${20 - label}`}/>
                            <Tooltip
                                labelFormatter={(label) => `${label}:${20 - label}`}/>
                            <Bar dataKey="total" fill={COLORS[0]} />
                        </BarChart>
                    </ResponsiveContainer>
                ) : (
                    <LoadingSpinner />
                )}
            </div>
        </article>
    )
}

export default StatsDistributionResults;