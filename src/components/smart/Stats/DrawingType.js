import React, { useEffect, useState } from "react";
import { PieChart, Pie, Sector, Cell, Legend, ResponsiveContainer } from 'recharts';
import Performance from "JS/rest/Performance";
import LoadingSpinner from "Dumb/Loading/Spinner";

const StatsDrawingType = ({ season, gametype }) => {
    const [drawings, setDrawings] = useState([]);
    const [promise, setPromise] = useState(true);
    const COLORS = ['#EB432D', '#F4AA24', '#4a828b', '#151515'];

    const RADIAN = Math.PI / 180;
    const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
        const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
        const x = cx + radius * Math.cos(-midAngle * RADIAN);
        const y = cy + radius * Math.sin(-midAngle * RADIAN);

        return (
            <text x={x} y={y} fill="#151515" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
                {`${(percent * 100).toFixed(0)}%`}
            </text>
        );
    };
    const renderLegend = ({ payload }) => {
        return (
            <ul className="stats-legend">
                {
                    payload.map((e, index) => (
                        <li key={`item-${index}`}>
                            <i style={{ backgroundColor: e.color }}></i>
                            {(e.payload._id) ? e.payload._id : 'undefined'} ({e.payload.total})
                        </li>
                    ))
                }
            </ul>
        );
    };

    useEffect(() => {
        setPromise(false);
        setDrawings(null);

        Performance.drawingtype(season, gametype).then(resp => {
            setDrawings(resp);
            setPromise(true);
        })
    }, [season, gametype]);

    return (
        <article className="stats-tile">
            <h1>Losverfahren</h1>
            <div className="chart">
                {(drawings && promise) ? (
                    <ResponsiveContainer>
                        <PieChart >
                            <Pie
                                data={drawings}
                                cx={140}
                                cy={100}
                                innerRadius={60}
                                outerRadius={80}
                                fill="#8884d8"
                                paddingAngle={5}
                                dataKey="total"
                            >
                                {drawings.map((entry, index) => (
                                    <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                                ))}
                            </Pie>

                            <Legend content={renderLegend} align="top" verticalAlign="top" />
                        </PieChart>
                    </ResponsiveContainer>
                ) : (
                    <LoadingSpinner />
                )}
            </div>
        </article>
    )

}

export default StatsDrawingType;