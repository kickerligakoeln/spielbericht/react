import React, {useEffect, useState} from "react";
import Performance from "JS/rest/Performance";
import TeamDetails from "Smart/Stats/TeamDetails";
import _ from "lodash";

import ChartBarIcon from "Images/icons/chart-bar.svg"

const StatsTeamPerformance = ({season, gametype}) => {
    const [performance, setPerformance] = useState(null);
    const [promise, setPromise] = useState(true);
    const [teamDetails, setTeamDetails] = useState(null);

    useEffect(() => {
        setPromise(false);
        setPerformance(null);

        Performance.teams(season, gametype)
            .then(response => {
                setPerformance(response);
                setPromise(true);
            });
    }, [season, gametype]);


    const toggleTeamDetails = (e, team) => {
        _.merge(team, {
            season, gametype
        })

        setTeamDetails(team);
    }

    return (
        <div>
            <TeamDetails team={teamDetails} emit={toggleTeamDetails} />

            {(performance && promise) ? (
                <div className="table -tiles">
                    <div className="table-head-stats">
                        <div className="index">#</div>
                        <div className="name">Team</div>
                        <div className="games -ellipsis">Spielberichte</div>
                        <div className="set -ellipsis">ø Sätze</div>
                        <div className="goals -ellipsis">ø Tore</div>
                        <div className="player -ellipsis">ø Player</div>
                        <div className="options"></div>
                    </div>

                    {performance.map((row, index) => (
                        <div className={`table-row-stats`}
                             onClick={(e) => toggleTeamDetails(e, row)}
                             key={index}>
                            <div className="index">{index + 1}.</div>
                            <div className="name">
                                <strong>{row._id}</strong>
                            </div>
                            <div className="games">{row.totalScoresheets}</div>
                            <div className="set">{(row.totalSets / row.totalScoresheets).toFixed(2)}</div>
                            <div className="goals">{(row.totalGoals / row.totalScoresheets).toFixed(2)}</div>
                            <div className="player">{(row.totalPlayers / row.totalScoresheets).toFixed(2)}</div>
                            <div className="options">
                                <ChartBarIcon className="icon" />
                            </div>
                        </div>
                    ))}
                </div>
            ) : (
                <div>Keine Daten vorhanden.</div>
            )}
        </div>
    )
}

export default StatsTeamPerformance;