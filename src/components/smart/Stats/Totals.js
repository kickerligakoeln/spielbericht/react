import React, { useEffect, useState } from "react";
import Performance from "JS/rest/Performance";
import ChartNumber from "Dumb/Chart/Number";
import ChartDistribution from "../../dumb/Chart/Distribution";

const StatsTotals = ({ season, gametype }) => {
    const [total, setTotal] = useState(null);
    const [promise, setPromise] = useState(true);
    const elseValue = (promise) ? '-' : null;

    /**
     * round number with 2 decimal places
     * @param {*} value 
     * @param {*} decimal 
     * @returns 
     */
    function round(value) {
        return Math.round((value + Number.EPSILON) * 100) / 100
    }


    /**
     * seconds to hh:mm:ss
     * @param {*} value 
     * @returns 
     */
    function mutateTime(value) {
        let mutatedTime = "-";

        if (value) {
            const sec_num = parseInt(value, 10); // don't forget the second param
            let days = Math.floor(sec_num / 3600 / 24);
            let hours = Math.floor((sec_num - (days * 24 * 3600)) / 3600);
            let minutes = Math.floor((sec_num - (days * 24 * 3600) - (hours * 3600)) / 60);
            let seconds = sec_num - (days * 24 * 3600) - (hours * 3600) - (minutes * 60);

            if (days !== 0) { days += " - " } else { days = ''; }
            if (hours < 10) { hours = "0" + hours; }
            if (minutes < 10) { minutes = "0" + minutes; }
            if (seconds < 10) { seconds = "0" + seconds; }
            mutatedTime = days + hours + ':' + minutes + ':' + seconds;
        }

        return mutatedTime;
    }


    useEffect(() => {
        setPromise(false);
        setTotal(null);

        Performance.total(season, gametype).then(total => {
            setTotal(total);
            setPromise(true);
        });
    }, [season, gametype]);


    return (
        <section className="stats-totals">
            <div className="container" data-grid="3">
                <ChartNumber
                    name="scoresheets"
                    label="Spielberichte"
                    value={(total && total.scoresheets) ? total.scoresheets : elseValue} />

                <ChartNumber
                    name="matches"
                    label="Spiele"
                    value={(total && total.matches.length) ? total.matches[0].count : elseValue} />

                <ChartNumber
                    name="goals"
                    label="Tore"
                    value={(total && total.goals.length) ? total.goals[0].total : elseValue} />

                <ChartNumber
                    name="players"
                    label="Spieler"
                    value={(total && total.players.length) ? total.players[0].total : elseValue}
                    avarage={(total && total.players.length) ? round(total.players[0].avarage) : elseValue} />

                <ChartNumber
                    name="time"
                    label="Spieldauer"
                    value={(total && total.time.length) ? mutateTime(total.time[0].total) : elseValue}
                    avarage={(total && total.time.length) ? mutateTime(total.time[0].median) : elseValue} />

                <ChartDistribution
                    goals={(total && total.goals.length) ? total.goals[0] : elseValue}
                    players={(total && total.players.length) ? total.players[0] : elseValue}
                    />
            </div>
        </section>
    );

};

export default StatsTotals;