import React from "react";
import {connect} from "react-redux";
import {Match} from "Dumb/Match/Match";
import Scoresheet from "JS/rest/Scoresheet";

/*
    TODO: name it "RecommendGames" with existing endpoint
        create LiveGames with different endpoint
 */
class LiveGames extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            matches: []
        }
    }

    componentDidMount() {
        const {season, gameType, gameday, league} = this.props;

        Scoresheet.live.running({season, gameType, gameday, league}).then(resp => {
            let matches = (resp.length) ? resp[0]['data'] : [];
            this.setState({matches});
        });
    }


    render() {
        if (!this.state.matches) {
            return null;
        } else {
            return (
                <div className="live-games">
                    {(Object.keys(this.state.matches).length > 0) ? (
                        <Match matches={this.state.matches}/>
                    ) : (
                        <div className="empty-state">Keine laufenden Matches</div>
                    )}
                </div>
            );
        }
    }
}

const mapStateToProps = state => ({
    scoresheetStore: state.scoresheetStore
});

export default connect(mapStateToProps, {})(LiveGames);