import React from "react";
import {useHistory} from "react-router-dom";
import {useSelector, useDispatch} from "react-redux";
import {buildScoresheetStore} from "Redux/scoresheetStore";
import {showNotification} from "Redux/notificationStore";

import {useAuth0} from "@auth0/auth0-react";
import {UserRole} from "JS/enums/UserRole";
import {NotificationType} from "JS/enums/NotificationType";
import {MatchState} from "JS/enums/MatchState";
import User from "JS/utils/User";
import Editor from "JS/rest/Editor";
import Scoresheet from "JS/rest/Scoresheet";
import Status from "JS/rest/Status";

import "./edit-scoresheet.scss";

export function AdminEditScoresheet() {
    const dispatch = useDispatch();
    const history = useHistory();
    const {user, isAuthenticated, getAccessTokenSilently} = useAuth0();
    const scoresheet = useSelector((state) => state.scoresheetStore.scoresheet);

    /**
     * edit scoresheet
     * - create editor token
     * - update MatchState in scoresheet
     * - update userReport in scoresheet
     * - dispatch scoresheet to redux
     * - route to /enter
     */
    function updateScoresheet() {
        let data = MatchState.IN_PROGRESS;

        if (scoresheet.matches.length === 0) {
            data = MatchState.DRAW_MATCHES;
        }

        if (!scoresheet.team_home.player || !scoresheet.team_guest.player) {
            data = MatchState.SETUP_PLAYERS;
        }

        Editor.set(scoresheet.matchid).then(token => {
            dispatch(buildScoresheetStore({
                build: 'state',
                data
            }));

            dispatch(buildScoresheetStore({
                build: 'user',
                data: true
            }));

            history.push('/enter');
        });
    }

    function approveScoresheet() {
        getAccessTokenSilently().then(access_token => {
            Scoresheet.approve(access_token, scoresheet.matchid)
                .then(() => {
                    dispatch(showNotification({
                        type: NotificationType.SUCCESS,
                        message: 'Scoresheet was successfully approved'
                    }));

                    history.push('/history')
                });
        });
    }

    /**
     * remove a scoresheet
     */
    function deleteScoresheet() {
        getAccessTokenSilently().then(access_token => {
            Scoresheet.delete(access_token, scoresheet.matchid)
                .then(() => {
                    dispatch(showNotification({
                        type: NotificationType.SUCCESS,
                        message: 'Scoresheet was successfully deleted'
                    }));

                    history.push('/history')
                });
        });
    }

    return (
        isAuthenticated &&
        User.hasRole(user, UserRole.ADMIN) && (
            <div className="edit-scoresheet">
                <ul>
                    <li>
                        <button onClick={updateScoresheet} data-icon="edit">Bearbeiten</button>
                    </li>
                    <li>
                        <button onClick={approveScoresheet} data-icon="submit">Beenden</button>
                    </li>
                    <li>
                        <button onClick={deleteScoresheet} data-icon="bin">Löschen</button>
                    </li>
                </ul>
            </div>
        )
    )
}
