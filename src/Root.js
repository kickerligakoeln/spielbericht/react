import React, { useEffect } from 'react'
import App from "./App";

import { useHistory } from 'react-router-dom'
import Tracking from "JS/rest/Tracking";

export const Root = () => {
    const history = useHistory()

    useEffect(() => {
        return history.listen((location) => Tracking.page(location.pathname));
    },[history])

    return (
        <App />
    )
}