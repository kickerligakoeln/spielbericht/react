// TODO: use other configs, too
export const KKL = {
    MIN_PLAYERS: 4,
    MAX_PLAYERS: 15,
    AMOUNT_MATCHES: 10,
    AMOUNT_SETS: 2,
    MAX_SCORE: 5
};
