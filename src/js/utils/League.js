import {GameType} from "../enums/GameType";

export default class League {

    /**
     *
     * @param key
     */
    shortName(key) {
        return (key !== GameType.CUSTOM) ? key.substring(5).toUpperCase() : '??';
    }
}