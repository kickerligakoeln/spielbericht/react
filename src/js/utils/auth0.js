import axios from "axios";

axios.interceptors.request.use(
    function (config) {
        return config;
    },
    function (error) {
        console.error('# auth0', error)
    });

export default axios;
