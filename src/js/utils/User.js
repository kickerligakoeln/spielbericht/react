export default class User {

    static identifier = 'https://kkl.api.module/roles';


    /**
     * roles have to be added in user object
     * https://manage.auth0.com/dashboard/eu/kkl/rules
     *
     * @param user
     * @param role
     * @returns {boolean}
     */
    static hasRole = (user, role) => {
        let hasRole = false;

        if (user) {
            hasRole = user[User.identifier].indexOf(role) !== -1;
        }

        return hasRole;
    }

}
