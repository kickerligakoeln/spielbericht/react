import Editor from "./Editor";
import Assert from "./Assert";
import Calculate from "./Calculate";
import {KKL} from "../setup/KKL";
import merge from "lodash.merge";
import md5 from "blueimp-md5";
import {MatchState, VisibleMatchStatus} from "JS/enums/MatchState";

export default class Factory {

    static set = {

        /**
         *
         * @param scoresheet
         * @param state
         * @returns {*}
         */
        state: (scoresheet, state) => {
            merge(scoresheet, {state});
            return scoresheet;
        },

        /**
         *
         * @param scoresheet
         * @param id
         * @returns {*}
         */
        id: (scoresheet, id) => {
            merge(scoresheet,
                {
                    matchInfo: {matchId: id}
                },
                {matchid: id}
            );
            return scoresheet;
        },

        /**
         *
         * @param scoresheet
         * @param data
         * @returns {*}
         */
        match: (scoresheet, data) => {
            merge(scoresheet,
                {
                    gameType: data.gameType,
                    league: data.leagueCode || null
                },
                {
                    matchInfo: {
                        leagueCode: data.leagueCode || null,
                        leagueName: data.leagueName || null,
                        currentSeason: data.currentSeason || null,
                        gamedayId: data.gamedayId || null,
                        gamedayNumber: data.gamedayNumber || null
                    }
                }
                // TODO: add user report here!
            );
            return scoresheet;
        },

        /**
         *
         * @param scoresheet
         * @returns {*}
         */
        date: (scoresheet) => {
            let date = scoresheet['date'];
            if (!date) {
                date = {
                    create: JSON.parse(JSON.stringify(new Date())),
                    update: null,
                    end: null
                };
            } else {
                date.update = JSON.parse(JSON.stringify(new Date()));
            }

            merge(scoresheet, {date});
            return scoresheet;
        },

        /**
         *
         * @param scoresheet
         * @param admin
         * @returns {*}
         */
        user: (scoresheet, admin = null) => {
            let user = scoresheet['user'];
            if (!user) {
                user = {
                    hash: md5('kkl-' + scoresheet.matchid),
                    report: [Editor.getReport()]
                }
            } else {
                user.report.push(Editor.getReport(admin));
            }

            merge(scoresheet, {user});
            return scoresheet;
        },

        /**
         *
         * @param scoresheet
         * @param data
         * @returns {*}
         */
        location: (scoresheet, data) => {
            let location = (!new Assert().isUndefined(data)) ? data : null;

            merge(scoresheet, {location});
            return scoresheet;
        },

        /**
         *
         * @param scoresheet
         * @param data
         * @returns {*}
         */
        team: (scoresheet, data) => {
            merge(scoresheet, {
                [data.type]: {
                    id: data.team.id || null,
                    logo: data.team.logo || null,
                    name: data.team.name || null
                }
            });
            return scoresheet;
        },

        /**
         *
         * @param scoresheet
         * @param data
         * @returns {*}
         */
        player: (scoresheet, data) => {
            scoresheet[data.team]['player'] = data.players

            return scoresheet;
        },

        /**
         *
         * @param scoresheet
         * @param data
         * @returns {*}
         */
        mail: (scoresheet, data) => {
            merge(scoresheet, {
                [data.type]: {
                    mail: data.team.captainEmail,
                    captain: data.team.captain || null
                }
            });

            return scoresheet;
        },

        /**
         *
         * @param scoresheet
         * @param data
         * @returns {*}
         */
        score: (scoresheet, data) => {
            scoresheet.date.update = JSON.parse(JSON.stringify(new Date()));
            scoresheet.matches[data.match].score[data.set][data.team] = parseInt(data.score);

            // set 5 points for winner
            if (data.score < KKL.MAX_SCORE) {
                let opponent = (data.team === 'home') ? 'guest' : 'home';
                scoresheet.matches[data.match].score[data.set][opponent] = KKL.MAX_SCORE;
            }

            scoresheet = Calculate.score(scoresheet);

            return scoresheet;
        }
    }

    static get = {

        /**
         * status of a scoresheet
         * @param scoresheet
         * @returns {string}
         */
        status: (scoresheet) => {
            let status = 'unknown';
            const isToday = (someDate) => {
                const today = new Date()
                return someDate.getDate() === today.getDate() &&
                    someDate.getMonth() === today.getMonth() &&
                    someDate.getFullYear() === today.getFullYear()
            }

            if (scoresheet.state === MatchState.IN_PROGRESS) {
                status = VisibleMatchStatus.LIVE;
            }

            if (scoresheet.state === MatchState.DONE || scoresheet.date.end) {
                status = VisibleMatchStatus.DONE;
            }

            if(
                scoresheet.status !== MatchState.DONE
                && !scoresheet.date.end
                && !isToday(new Date(scoresheet.date.update))
            ) {
                status = VisibleMatchStatus.WARNING
            }

            return status;
        }

    }
}
