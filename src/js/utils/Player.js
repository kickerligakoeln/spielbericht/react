export default class Player {

    static shortName(name) {
        const maxLenght = 12;
        let friendlyName = name;

        if (friendlyName.length > maxLenght) {
            let space = name.search(" ");

            if (space !== -1) {
                friendlyName = name.substr(0, space + 2) + '.';
            } else {
                friendlyName = friendlyName.substr(0, 9) + '...';
            }
        }

        return friendlyName;
    }
}