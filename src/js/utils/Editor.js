export default class Editor {


    /**
     *
     * @param admin
     * @returns {{vendor: string, admin: *, connection: Socket | string | tls.TLSSocket, userAgent: string, login: Date, resolution: {width: number, height: number}}}
     */
    static getReport = (admin = null) => {
        return {
            login: JSON.parse(JSON.stringify(new Date())),
            // connection: navigator.connection,
            userAgent: navigator.userAgent,
            vendor: navigator.vendor,
            resolution: {
                width: window.innerWidth,
                height: window.innerHeight
            },
            admin: admin
        }
    };
}