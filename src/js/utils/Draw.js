import merge from "lodash.merge";
import {DrawingType} from "../enums/DrawingType";
import {KKL} from "../setup/KKL";

export default class Draw {

    /**
     *
     * @param scoresheet
     * @returns {*}
     */
    static kkl = (scoresheet) => {
        let homePlayersCopy = [];
        let guestPlayersCopy = [];
        let matches = [];

        let createCopy = () => {
            if (homePlayersCopy.length === 0) {
                homePlayersCopy = scoresheet.team_home.player.slice();
                shuffle(homePlayersCopy);
            }
            if (guestPlayersCopy.length === 0) {
                guestPlayersCopy = scoresheet.team_guest.player.slice();
                shuffle(guestPlayersCopy);
            }
        };

        let shuffle = (a) => {
            let j, x, i;
            for (i = a.length; i; i--) {
                j = Math.floor(Math.random() * i);
                x = a[i - 1];
                a[i - 1] = a[j];
                a[j] = x;
            }
        };

        let tempName = '';
        for (let i = 0; i < KKL.AMOUNT_MATCHES; i++) {
            let player = {};
            createCopy(); //in case our stack is empty
            player.home_a = homePlayersCopy.pop().name;
            player.guest_a = guestPlayersCopy.pop().name;
            createCopy(); //in case our stack is empty

            let homeB = homePlayersCopy.pop().name;
            if (homeB === player.home_a) {
                //we don't want this!
                tempName = homeB;
                homeB = homePlayersCopy.pop().name;
                homePlayersCopy.push({
                    name: tempName
                })
            }

            player.home_b = homeB;
            let guestB = guestPlayersCopy.pop().name;
            if (guestB === player.guest_a) {
                //we don't want this!
                tempName = guestB;
                guestB = guestPlayersCopy.pop().name;
                guestPlayersCopy.push({
                    name: tempName
                });
            }

            player.guest_b = guestB;
            matches.push({
                player: player,
                score: [
                    {home: 0, guest: 0},
                    {home: 0, guest: 0}
                ]
            });
        }

        scoresheet.matches = matches;
        scoresheet.drawingType = DrawingType.AUTO;
        return scoresheet;
    }


    /**
     *
     * @type {{init: (function(*): *), set: (function(*, *, *, *): *)}}
     */
    static custom = {

        // generate empty matches
        init: (scoresheet) => {
            let matches = [];
            for (let i = 0; i < KKL.AMOUNT_MATCHES; i++) {
                matches.push({
                    player: null,
                    score: [
                        {home: 0, guest: 0},
                        {home: 0, guest: 0}
                    ]
                });
            }

            merge(scoresheet, {matches});
            return scoresheet;
        },

        // set player in matches
        set: (scoresheet, data) => {
            merge(scoresheet.matches[data.match],
                {player: {[data.teamPos]: data.player}}
            );

            scoresheet.drawingType = DrawingType.CUSTOM;
            return scoresheet;
        }
    }

}
