import axios from "axios";

export default class Performance {

    /**
     * fetch total stats per {season} and {gametype}
     * @returns {Promise<unknown>}
     * @param season
     * @param gametype
     */
    static total(season, gametype) {
        return new Promise(resolve => {
            axios.get(`/api/v2/performance/total/${season}/${gametype}`)
                .then(resp => resolve(resp['data']['result']));
        })
    }


    /**
     * fetch Drawing Types
     * @returns {Promise<unknown>}
     * @param season
     * @param gametype
     */
    static drawingtype(season, gametype) {
        return new Promise(resolve => {
            axios.get(`/api/v2/performance/drawingtype/${season}/${gametype}`)
                .then(resp => resolve(resp['data']['result']));
        })
    }


    /**
     * fetch Favorite Weekday
     * @returns {Promise<unknown>}
     * @param season
     * @param gametype
     */
    static favoriteWeekday(season, gametype) {
        return new Promise(resolve => {
            axios.get(`/api/v2/performance/weekday/${season}/${gametype}`)
                .then(resp => resolve(resp['data']['result']));
        })
    }


    /**
     * fetch player performance per scoresheet
     * @param matchid
     */
    static players(matchid) {
        return new Promise(resolve => {
            axios.get(`/api/v2/performance/player/${matchid}`)
                .then(resp => resolve(resp['data']['result']));
        })
    }


    /**
     * return player performance of a given teamname for a season & gametype
     * @param teamName
     * @param season
     * @param gameType
     * @returns {Promise<unknown>}
     */
    static playersOverall(teamName, season, gameType) {
        return new Promise(resolve => {
           axios.post(`/api/v2/performance/player-overall`, {
               team: teamName,
               season,
               gameType
           })
               .then(resp => resolve(resp['data']['result']));
        });
    }


    /**
     * fetch team performance
     * @param season
     * @param gametype
     * @returns {Promise<unknown>}
     */
    static teams(season, gametype) {
        return new Promise(resolve => {
            axios.get(`/api/v2/performance/team/${season}/${gametype}`)
                .then(resp => resolve(resp['data']['result']));
        })
    }


    /**
     * return distribution of results
     * @param season
     * @param gametype
     * @returns {Promise<unknown>}
     */
    static distributionResults(season, gametype) {
        return new Promise(resolve => {
            axios.get(`/api/v2/performance/results/${season}/${gametype}`)
                .then(resp => resolve(resp['data']['result']));
        })
    }


    /**
     * return head to head comparison
     * @param matchid
     * @returns {Promise<unknown>}
     */
    static headToHead(matchid) {
        return new Promise(resolve => {
            axios.get(`/api/v2/performance/head-to-head/${matchid}`)
                .then(resp => resolve(resp['data']['result']));
        })
    }

}
