import React from "react";
import axios from "axios";

import Store from "JS/utils/Store";

export default class Kickerliga {

    /**
     * table ranks of {season}
     * @param season
     * @returns {Promise<unknown>}
     */
    static ranks(season) {
        return new Promise(resolve => {
            axios.get(`/api/v2/league/ranks/${season}`)
                .then(resp => resolve(resp['data']['result']));
        });
    }


    /**
     * list of all Locations
     * @returns {Promise<unknown>}
     */
    static locations = () => {
        return new Promise(resolve => {
            axios.get(`/api/v2/league/locations`)
                .then(resp => resolve(resp['data']['result']));
        })
    }

    /**
     *
     * @returns {Promise<unknown>}
     */
    static activeLeagues = () => {
        return new Promise(resolve => {
            let storedActiveLeagues = Store.getSessionStorage('activeLeagues');

            if (storedActiveLeagues) {
                resolve(storedActiveLeagues);
            } else {
                axios.get(`/api/v2/league/active`)
                    .then(resp => {
                        Store.setSessionStorage('activeLeagues', resp['data']['result']);
                        resolve(resp['data']['result']);
                    });
            }
        });
    };


    /**
     *
     * @param gamedayId
     * @returns {Promise<unknown>}
     */
    static upcomingMatches(gamedayId) {
        return new Promise(resolve => {
            axios.get(`/api/v2/league/matches/${gamedayId}`)
                .then(resp => resolve(resp['data']['result']));
        });
    }


    /**
     *
     * @param teamId
     * @returns {Promise<unknown>}
     */
    static team(teamId) {
        return new Promise(resolve => {
            axios.get(`/api/v2/league/team/${teamId}`)
                .then(resp => resolve(resp['data']['result']));
        })
    }
}
