import React from "react";
import axios from "axios";
import Store from "JS/utils/Store";

export default class Cup {

    /**
     *
     * @returns {Promise<unknown>}
     */
    static matches() {
        return new Promise(resolve => {
            let storedMatches = Store.getSessionStorage('cupMatches');

            if (storedMatches) {
                resolve(storedMatches);
            } else {
                axios.get('/api/v2/cup/matches').then(resp => {
                    Store.setSessionStorage('cupMatches', resp['data']['result']);
                    resolve(resp['data']['result']);
                });
            }
        });
    }
}
