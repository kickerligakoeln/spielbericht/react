import axios from "axios";

export default class Tracking {

    /**
     * send page tracking data to application
     * @returns {Promise<unknown>}
     * @param pathname
     */
    static page(pathname) {
        return new Promise(resolve => {
            const data = {
                pathname,
                resolution: window.innerWidth,
                userAgent: navigator.userAgent
            };

            axios.post(`/api/v2/tracking/page`, data)
                .then(resp => resolve(resp['data']['result']));
        })
    }


    /**
     * send click tracking to application
     * @returns {Promise<unknown>}
     * @param context
     * @param target
     */
    static click(context, target) {
        return new Promise(resolve => {
            const data = {
                context,
                target
            };

            axios.post(`/api/v2/tracking/click`, data)
                .then(resp => resolve(resp['data']['result']));
        })
    }


    /**
     * send (http) errors to application
     * @param msg
     */
    static error(msg) {
        axios.post(`/api/v2/tracking/error`, msg);
    }

}
