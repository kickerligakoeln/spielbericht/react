import React from "react";
import axios from "axios";
import Store from "../utils/Store";
import {MatchState} from "JS/enums/MatchState";
import _ from "lodash"
import Tracking from "JS/rest/Tracking";

export default class Scoresheet {

    static get = {
        local: () => {
            return Store.getLocalStorage('scoresheet') || null;
        },

        id: (id) => {
            return new Promise(resolve => {
                axios.get(`/api/v2/scoresheet/${id}`)
                    .then(resp => resolve({
                        scoresheet: resp['data']['result'],
                        performance: resp['data']['performance']
                    }));
            })
        }
    }

    static set = {
        match: (scoresheet) => {
            return new Promise((resolve, reject) => {
                const editor = Store.getCookie('EDITOR_AUTH');
                axios.post(`/api/v2/scoresheet`, scoresheet, {headers: {'X-Editor': editor}})
                    .then(resp => resolve({
                        scoresheet: resp['data']['result'],
                        performance: resp['data']['performance']
                    }))
                    .catch(e => {
                        Tracking.error({e, editor, matchid: scoresheet.matchid, context: 'scoresheet.set.match'});
                        reject(e);
                    });
            })
        },

        finalize: (scoresheet) => {
            return new Promise(async resolve => {
                const editor = Store.getCookie('EDITOR_AUTH');
                let s = _.merge({},
                    scoresheet,
                    {
                        state: MatchState.DONE,
                        date: {end: JSON.parse(JSON.stringify(new Date()))}
                    }
                );

                let resp = await axios.post(`/api/v2/scoresheet`, s, {headers: {'X-Editor': editor}})
                    .catch(e => {
                        Tracking.error({e, editor, matchid: scoresheet.matchid, context: 'scoresheet.set.finalize'});
                    });

                await axios.delete(
                    `/api/v2/editor/${s.matchid}`,
                    {headers: {'X-Editor': Store.getCookie('EDITOR_AUTH')}});

                resolve(resp['data']['result']);
            })
        }
    }


    /**
     *
     * @param access_token
     * @param matchid
     * @returns {Promise<unknown>}
     */
    static delete = (access_token, matchid) => {
        return new Promise(resolve => {
            axios.delete(`/api/v2/scoresheet/${matchid}`, {
                headers: {'Authorization': `Bearer ${access_token}`}
            })
                .then(resp => resolve(resp['data']));
        });
    }


    static approve = (access_token, matchid) => {
        return new Promise((resolve, reject) => {
            axios.post(
                `/api/v2/scoresheet/approve/${matchid}`,
                null,
                {
                    headers: {'Authorization': `Bearer ${access_token}`}
                }
            )
                .then(resp => resolve(resp['data']))
                .catch(e => reject(e));
        })
    }


    static live = {
        /**
         *
         * @param gameType
         * @param season
         * @returns {Promise<unknown>}
         */
        all: (gameType, season) => {
            return new Promise(resolve => {
                axios.get(`/api/v2/scoresheet/matches/${season}/${gameType}`)
                    .then(resp => resolve(resp['data']['result']));
            })
        },

        /**
         *
         * @param filter
         * @returns {Promise<unknown>}
         */
        running: (filter = {}) => {
            return new Promise(resolve => {
                axios.get(`/api/v2/scoresheet/matches/${filter.season}/${filter.gameType}/${filter.gameday}/${filter.league}`)
                    .then(resp => resolve(resp['data']['result']));
            })
        }

    }

}
