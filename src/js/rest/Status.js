import axios from "axios";

export default class Status {

    static test = (access_token) => {
        return new Promise(resolve => {
            axios.get(`/api/v2/status/account`, {
                headers: {'Authorization': `Bearer ${access_token}`}
            })
                .then(resp => resolve(resp['data']));
        });
    }

}
