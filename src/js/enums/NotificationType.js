export const NotificationType = {
    HINT: 'hint',
    ERROR: 'error',
    SUCCESS: 'success'
}