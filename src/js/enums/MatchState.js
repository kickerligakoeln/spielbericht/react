export const MatchState = {
    SETUP: 'SETUP',
    SETUP_PLAYERS: 'SETUP_PLAYERS',
    DRAW_MATCHES: 'DRAW_MATCHES',
    IN_PROGRESS: 'IN_PROGRESS',
    CONFIRM: 'CONFIRM',
    DONE: 'DONE'
};

export const VisibleMatchStatus = {
    LIVE: "live",
    DONE: "done",
    WARNING: "warning"
}