import React from 'react';
import ReactDOM from 'react-dom';
import {store} from "Redux/store"
import "JS/utils/i18n";
import "JS/utils/auth0";
import pckgJson from '/package.json';

import {HashRouter} from "react-router-dom";
import {Helmet} from "react-helmet";
import {Provider} from 'react-redux'
import {Auth0Provider} from "@auth0/auth0-react";
import {Root} from "./Root";

import './css/base.scss';

ReactDOM.render(
    <HashRouter>
        <Helmet>
            <title>Digitaler Spielberichtsbogen | Kölner Kickerliga</title>
            <meta property="version" content={pckgJson.version}/>
            <meta name="timestamp" content={new Date()}/>

            <meta name="auth0" content={process.env.AUTH0_DOMAIN}/>

            <meta property="og:title" content="Der digitale Spielberichtsbogen - Kölner Kickerliga"/>
            <meta property="og:url" content={window.location.origin}/>
            <meta property="og:description"
                  content="Kein bock zu rechnen? Kein Stift zur Hand? Spielbogen vergessen? Mit dieser kleinen Webseite könnt ihr bequem und umweltbewusst euren Spielberichtsbogen an einem internetfähigen Gerät eurer Wahl ausfüllen."/>
            <meta property="og:type" content="website"/>
            {/*<meta property="og:image" content={imgShare}/>*/}
        </Helmet>

        <Provider store={store}>
            <Auth0Provider
                domain={process.env.AUTH0_DOMAIN}
                clientId={process.env.AUTH0_CLIENT_ID}
                redirectUri={window.location.origin}
                useRefreshTokens={true}
                cacheLocation="localstorage"
                audience={process.env.AUTH0_AUDIENCE}
                scope="scoresheet:edit"
            >
                <Root />
            </Auth0Provider>
        </Provider>

    </HashRouter>,
    document.getElementById('root')
);
