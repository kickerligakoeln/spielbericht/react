# Digitaler Spielberichtsbogen

Die <a href="https://www.kickerligakoeln.de/koelnerkickerliga/was-ist-die-kkl/" target="_blank">Kölner Kickerliga</a>
(KKL) ist eine _„bunte Liga“_, also selbstständig organisiert und unabhängig vom Spielbetrieb des Deutschen
Tischfußballbunds DTFB. 2010 gegründet, lautet das Motto der Kölner Kickerliga:
_„Crack oder Einsteiger? Egal – Hauptsache Bock auf Zocken“_.

Ein Spieltag besteht aus 10 Spiele á 2 Sätze. Die Losung für eine solche Begegnung ist dabei besonders:
am Austragungsort werden vor der ersten Partie alle 10 Paarungen des gesamten Spieltags ausgelost werden. Das fördert
den Mannschaftsgedanken als auch das faire Aufeinandertreffen zweier unterschiedlich starker Teams.

Der <a href="https://spiel.kickerligakoeln.de/" target="_blank">digitale Spielberichtsbogen</a> soll dabei den
Spielbetrieb am Spieltag unterstützen. Aufstellung, Losung, Berechnung und Mitteilung wird dabei komplett automatisiert.

## Requirements

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

* [Docker](https://www.docker.com/) v2.0.0.2
* [Node Version Manager](https://github.com/nvm-sh/nvm)
* [Node](https://nodejs.org/en/) v14.7.0

#### setup environment

```bash
$ cp docs/example.env .env
```

#### build / run application

```bash
$ nvm use
$ npm install

# generate static files | start watcher
$ npm run build|dev
```

find your web service here: http://localhost:3000

## Authentifizierung

### Spielleiter

Ein Nutzer der ein neues Spiel startet, wird in der Applikation automatisch als **Spielleiter** registriert. Dafür wird
vom Backend eine `uuid` (v4) generiert, mit der MatchId gemeinsam in der Datenbank abgelegt und beim Spielleiter selbst
als Cookie (`EDITOR_AUTH`) gespeichert. Bei allen zukünftigen API Anfragen wird dieser als Custom-Header `X-Editor`
mitgeschickt. Das Backend kann daraufhin den Spielleiter authentifizieren und den Request zulassen bzw verweigern.

Meldet sich ein zweiter Spielleiter nachträglich über das Formular an, wird ein weiterer Eintrag in der Datenbank
angelegt.

Mit Beenden des Spiels werden die Validierungsdaten automatisch aus der Datenbank gelöscht.

### Admin

Über eine erfolgreiche Admin Authentifizierung können Spiele auch von einer zweiten Person (nachträglich) bearbeitet
oder gelöscht werden. Außerdem sind alle (auch unbeendeten) Spiele einsehbar. Dafür muss ein **auth0** Account erstellt
und die entsprechende Rolle **"Admin"** vergeben werden.

## Dict

| name                      | description                                                                              |
|---------------------------|------------------------------------------------------------------------------------------|
| match / Begegnung         | Die reine Information über das Aufeinandertreffen zweier Teams (Quelle: Ligatool)        |
| scoresheet / Spielbericht | Eine durch den digitalen Spielberichtsbogen abgeschlossene Begegnung                     |
| game / Spiel              | Ein Spiel wird im 2vs2 gespielt und besteht aus 2 Sätzen. Es kann unentschieden ausgehen |
| set / Satz                | Ein Satz wird immer bis max. 5 Tore gespielt                                             |

## Changelog

#### 2022

* player performances
* team performances
* head-to-head performances
* ci/cd by gitlab

#### 2021

* use redux
* implement [auth0](https://auth0.com/de) for user registration.
* assign `roles` and `permissions` (eg admin) by auth0
* facelifting
* automated deployment by [gitlab ci/cd](https://gitlab.com/kickerligakoeln/spielbericht/react/-/pipelines)
* use MongoDB instead of MongoLite

#### 2020

* develope a brand new app. Build with [React](https://reactjs.org/).
* integrate matches from [challonge](https://challonge.com/de/dashboard.html)

#### 05/2019

* show userAgent in `/statistics`

#### 03/2019

* share game-url on whatsapp, fb-messanger and telegram
* copy game-url to clipboard

#### 11/2018

* show live games for current gameday und league in `/live`

#### 10/2018

* use json-files for validation to relieve MongoLite DB
* show live matches on index
* load phpMailer by composer
* mail preview: `/mail/preview?match=<matchId>`

#### 07/2018

* use kkl api-key to get user data (e-mail)
* hide user data (e-mail) in console.logs
* send score-data to kkl-ligatool (for "liveRanking")
* refactor `config.php`
* make it easier to update icons

#### 05/2018

* show cup in `/overview.php`
* use *Single-Score-Modus* to edit single matches
* remove captain e-mail address from scoresheet after sending mail
* admin navigation: show **all** scoresheets
* admin navigation: send scoresheet by email
* add *Overlays* (siehe Anleitung)
* authentification by cookie

#### 04/2018

* use <a href="https://github.com/agentejo/mongo-lite">MongoLite DB</a> to store scoresheets. (See documentation above.)
* add Database Interface
* migrate & fix old json-files
* admin authentication by cookie
* admin navigation: edit/remove scoresheets in frontend

#### 03/2018

* redesign navigation

#### 02/2018

* use new "KKL JSON API"
* refactor endpoints, rename methods
* redesign matchDetails (`/step3`, `/live`)
* new HTML e-mail template by <a href="https://maildeveloper.com/">Mail Developer</a>

#### 01/2018

* tiny matchtitle while scrolling
* season history in `/overview` and `/statistics`
* redesign social sharing

#### 12/2017

* integrate casperJS for testing

#### 11/2017

* show location in google maps (`live.php`)
* hide `.php` in URL (*"RewriteRule"*)
* update folder structure and resources

#### 10/2017

* show *"player statistics"*, games, rate
* check for `localStorage` and show error Message

#### 09/2017

* add `statistics.php`
* add mail *pokal@kickerligakoeln.de* when `$cup === true`

#### 06/2017

* show *"game trend"*, highlight winner
* init `quicklab.php`
* ref html (no twitter bootstrap)
* ref buttons
* redesign `overview.php`
* highlight running games (Live!)
* `overview.php` -> use angular controller

#### 05/2017

* advanced user report in scoresheet
* less to sass
* `grunt postcss`
* use https://icomoon.io/

#### 04/2017

* use ITCSS structure (https://www.xfive.co/blog/itcss-scalable-maintainable-css-architecture/)
* dynamic metatags
* add social share buttons
* init manager login
* use md5 hashes for manager login
* redesign Landingpage

#### 02/2017

* move to https
* grunt `composer:install`
* send data to kkl tool
* push notification by _www.onesignal.com_

#### 01/2017

* new gametype: "Freundschaftsspiel"
* use sudden death

#### 09/2016

* add google analytics
* swipe in `step3.php` and `live.php` to see matchdetails or live table
* bugfixes

#### 08/2016

* integrate websocket
* add angularJS
* HTML5 manifest

#### 07/2016

* add _https://www.foaas.com_ for loading time
* refactor templates

#### 04/2016

* import _digitaler spielberichtsbogen_ to github
* config spiel.kickerligakoeln.de
* First implementation of a remote result display
* add grunt for css compiling and css / js minifying
* refactor css/less
