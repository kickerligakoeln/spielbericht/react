`Stand 07/2022`

--

# Scoresheet

```typescript
{
    "_id": {
        "$oid": String
        },
    "date": {
        "create": Timestamp,
        "update": Timestamp,
        "end": Timestamp | null
    },
    "state": MatchState,
    "matchInfo": {
        "matchId": Number,
        "leagueCode": Number | null,
        "leagueName": String | null,
        "currentSeason": Number | null,
        "gamedayId": Number | null,
        "gamedayNumber": Number | null
    },
    "matchid": Number,
    "user": {
        "hash": String,
        "report": [
            {
                "login": Timestamp,
                "userAgent": String,
                "vendor": String,
                "resolution": {
                    "width": Number,
                    "height": Number
                },
                "admin": Boolean | null
            }
        ]
    },
    "gameType": GameType,
    "league": String | null,
    "team_home": {
        "id": Number | null,
        "logo": String | null,
        "name": String,
        "mail": String,
        "captain": String | null,
        "player": {"name": String,"score": Number | null}[],
        "goals": Number,
        "set": Number
    },
    "team_guest": {
        "id": Number | null,
        "logo": String | null,
        "name": String,
        "mail": String,
        "captain": String | null,
        "player": {"name": String,"score": Number | null}[],
        "goals": Number,
        "set": Number
    },
    "location": Any,
    "drawingType": DrawingType,
    "matches": {
        "player":{ 
            "home_a": String, 
            "guest_a": String,
            "home_b": String,
            "guest_b": String
        },
        "score": {
            "home": Number,
            "guest": Number
        }[]
    }[],
    "winner": {
        "match": Number,
         "team": String,   
         "set": Number   
    } | null
}
```